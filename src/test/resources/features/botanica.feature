Feature: Botanica Booking

   Scenario: Botanica Request broucher
    Given user on the Botanica page
    When user click on broucher for "botanica"
    And user enter details
      | Miss | Test | User | India | ACT | Melbourne | Mel | 1111 | 1234567890 | pooja.kanapuram@aptouring.com.au |
    Then quote is successful


  Scenario: Botanica News Letter Signup
    Given user on Botanica page
    When user provides details
      | Mr | Test | User | pooja.kanapuram@aptouring.com.au |
    And clicks on SignUp
    Then user will be signed up

  Scenario: Botanica Search trip by filter
    Given user on Botanica
    When user search trip by filter
      | Australia | History | a week |
    Then validate results
      | australia | 20 |


  Scenario: Botanica trip By Special Deals
    Given user is on Botanica page
    When user search trip
    Then search "Europe & UK" trip by Special Deals "Early booking deal - Save $1000 per couple*"

  Scenario: Botanica Contact Us
    Given user on Botanicapage
    When user click on contactus
    Then select "Bookings"
    And enters message "message"
    And enters details
      | Miss | Test | User | India | 1111 | ACT | 1234567890 | pooja.kanapuram@aptouring.com.au |
    Then email will be sent "Europe & UK" trip by Special Deals "Air Credit $1,000 per couple*"
    
    
      Scenario: Botanica Booking
    Given user on Botanica Website
    When user search by trip code "btnztm9"
    When choose trip
    And user choose availability
      | 31 | Oct | Sun |
    And user enter room "Double"
    And user enter detail
      | Miss | Test | User | pooja.kanapuram@aptouring.com.au | 11111111 | Miss | Test | User |
    Then user enters payment details and Payment should be successful
      | full_amount | Credit | TestUser | 5555555555554444 | 05/21 | 654 |
    