@tag
Feature: Travel Marvel Booking

  Scenario: Request broucher for TM
    Given user on the TM page
    When user click on TM Post Me A broucher for "Canada"
    And user should enter details on TM
      | Miss | Test | User | India | ACT | Melbourne | Mel | 1111 | 1234567890 | pooja.kanapuram@aptouring.com.au |
    Then quote should be successful on TM

  Scenario: TM News Letter Signup
    Given user on TM page
    When user provides details on TM
      | Mr | Test | User | pooja.kanapuram@aptouring.com.au |
    And clicks on SignUp for TM
    Then user will be signed up for TM

  Scenario: TM Search trip by filter
    Given user is on TM page
    When user search trip by filter for TM
      | Australia | History | a week |
    Then validate results for TM
      | australia | 20 |

  Scenario: TM trip By Special Deals
    Given user is on TM Website HomePage
    When user search trip for TM page
    Then search trip "Asia" by Special Deals "Fly Free + Save $600 per couple*"

  Scenario: TM Contact Us
    Given user is on the TM page
    When user click on contact us for TM Page
    Then select for TM Page "Bookings"
    And enters message for TM Page "message"
    And enters details for TM Page
      | Miss | Test | User | India | 1111 | ACT | 1234567890 | pooja.kanapuram@aptouring.com.au |
    Then email will be sent for TM Page

  Scenario: TM Trip Booking
    Given User on TM HomePage
    When user search by trip code for TM Page "EUTCOP08"
    When choose trip for TM Page
    And user choose availability for TM Page
      | 3 | Aug | Tue |
    And user enter room for TM Page "Double"
    And user enter details for TM Page
      | Miss | Test | User | pooja.kanapuram@aptouring.com.au | 11111111 | Miss | Test | User |
    Then user enters payment details for TM Page and Payment should be successful
      | full_amount | Credit | TestUser | 5555555555554444 | 05/21 | 654 |
