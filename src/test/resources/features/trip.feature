#Author: pooja.kanapuram@aptouring.com.au
@tag
Feature: APTouring Booking.

	

 @tripsearch
  Scenario: Filter trips for Booking
    Given user on apt home page
    When user search trip by filters
      | Asia | Land Journey | two weeks |
    Then validates results
      | asia | 2020 |

  @newsletter
  Scenario: News Letter Signup
    Given user on the APT page
    When user enter the details
      | Mr | Test | User | pooja.kanapuram@aptouring.com.au |
    And click on Signup
    Then user should be signed up

  

  @contactus
  Scenario: Contact Us
    Given user on APT Home page
    When user clicks on contactus
    Then select Topic "Bookings"
    And enter message "message"
    And enter details
      | Miss | Test | User | India | 1111 | ACT | 1234567890 | pooja.kanapuram@aptouring.com.au |
    Then email should be sent

  @requestbrochure
  Scenario: Request broucher
    Given user on APTouring page
    When user click on Post Me A broucher for "Asia"
    And user should enter details
      | Miss | Test | User | India | ACT | Melbourne | Mel | 1111 | 1234567890 | pooja.kanapuram@aptouring.com.au |
    

  @specialdeals
  Scenario: Book trip By Special Deals
    Given user is on APT page
    When user searchs trip
    Then search the "South America & Antarctica" trip by Special Deals "Fly Free including taxes*"


    @tripbooking
  Scenario: APT Booking for the trip Europe
    Given user on APT page
    When user enter trip code "inkrdv17"
    Then user should see the trip "Scandinavian Rail and Fjords"
    Then user clicks on the trip
    And user enters date
      | 22 | Mar | Mon |
    Then user selects roomtype "Twin" and cabin ""
    #Then user selects promotions and continue
    Then user enters personal details
      | Miss | Test | User | pooja.kanapuram@aptouring.com.au | 11111111 | Miss | Test | User |
    Then user enter paymentdetails and Payment should be successful
      | full_amount | Debit | TestUser | 5555555555554444 | 05/21 | 654 |