package listeners;
/////////////////// Testing now
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import static stepDefinations.BaseClassSteps.featureName;

import java.io.File;
import java.io.IOException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;


import base.TestBase;

public class ExtentReportListener { 
	
	  public static ExtentHtmlReporter report= null;
	  
	  public static ExtentReports extent= null;
	  
	  public static ExtentTest test= null;
	  
	  public static ExtentReports setUp() { //First is to create Extent Reports
	  String reportLocation = "./reports1/Extent_Report.html"; 
	  report= new ExtentHtmlReporter(reportLocation);
	  report.config().setDocumentTitle("Test Report for Appium");
	  report.config().setReportName("Test Report");
	 
	  report.config().setTheme(Theme.DARK); report.config().setEncoding("utf-8");
	  System.out.println("************Extent Report location initialized***************"); 
	  report.start();
	  
	  extent = new ExtentReports(); extent.attachReporter(report);
	  extent.setSystemInfo("Application", "APTTouring");
	  extent.setSystemInfo("Operating System", System.getProperty("os.name"));
	  extent.setSystemInfo("UserName", System.getProperty("user.name"));
	  extent.createTest("Test").assignCategory("requestbrochure").pass("details");
	  System.out.println("System info. set in Extent Reports"); return extent;
	  
	  }
	  
	  
	  
	  public static void testStepHandle(String teststatus,WebDriver
	  driver,ExtentTest extenttest,Throwable throwable) { switch (teststatus) {
	  case "FAIL":
	  extenttest.fail(MarkupHelper.createLabel("Test Case is Failed : ",
	  ExtentColor.RED)); extenttest.error(throwable.fillInStackTrace());
	  
	  
	  
	  if (driver != null) { driver.quit(); } break;
	  
	  case "PASS":
	  extenttest.pass(MarkupHelper.createLabel("Test Case is Passed : ",
	  ExtentColor.GREEN)); break;
	  
	  default: break; } }
	  
	  public void ExtentReportScreenshot() throws IOException {
	  
	  TakesScreenshot screen = (TakesScreenshot) TestBase.driver; File src =
	  screen.getScreenshotAs(OutputType.FILE); String dest
	  ="./reports1/screenshots/" + getcurrentdateandtime() +".png"; File target =
	  new File(dest); FileUtils.copyFile(src, target); }
	  
	  
	  private static String getcurrentdateandtime() { String str = null; try {
	  DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss:SSS"); Date
	  date = new Date(); str = dateFormat.format(date); str = str.replace(" ",
	  "").replaceAll("/", "").replaceAll(":", ""); } catch (Exception e) { } return
	  str; }
	  
	  
	  public void FlushReport()
	  { extent.flush(); }
	  
	  
	  
	  public static String captureScreenShot(WebDriver driver) throws IOException {
	  TakesScreenshot screen = (TakesScreenshot) driver; File src =
	  screen.getScreenshotAs(OutputType.FILE); String dest
	  ="./reports/"+featureName+"/screenshots/" + getcurrentdateandtime() + ".png";
	  File target = new File(dest); FileUtils.copyFile(src, target); return dest; }
	  
	  
}
	 
