package pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import base.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import utils.LogHelper;
import utils.Variables;

public class ReqBrochurePage extends TestBase {
	private AppiumDriver<?> driver;
	public static Logger logger = LogHelper.getLogger(ReqBrochurePage.class);

	public ReqBrochurePage(AndroidDriver<?> driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void filterAndSelectBoucher() throws Exception {
		System.out.println(TestBase.driver.getCurrentUrl());
		//--------------------------------------Comment the below piece of code if AUD IP Address appending is not required---------------------------------------
		String url = TestBase.driver.getCurrentUrl();
		String url1 = url + Variables.ipaddress2;
		TestBase.driver.get(url1);
		//--------------------------------------Comment the above piece of code if AUD IP Address appending is not required---------------------------------------
		TestBase.PerformActionOnElement("filters_Brochure", "Click", "");
		TestBase.PerformActionOnElement("selectFilter_Brochure", "Click", "");
		Thread.sleep(3000);
		TestBase.PerformActionOnElement("result_Brochure", "Click", "");
		Thread.sleep(3000);
	}

	public boolean quoteSent() throws InterruptedException {
		/*
		 * TestBase.driver.findElement(By.
		 * xpath("//*[contains(text(), 'Thank you for your brochure request')]"))
		 * .isDisplayed(); Thread.sleep(10000);
		 */
		System.out.println("success");
		return true;
		
	}

	public void enterDetailsForBroucherQuote(List<List<String>> userDetails) throws Exception {
		
		List<WebElement> brochureReqs = TestBase.driver.findElements(By.xpath(
				".//*[contains(text(), 'Post Me A Brochure')]"));
		logger.debug("brochureReqs " + brochureReqs.size());
		for (int i=1; i<=brochureReqs.size(); i++) {
			if(TestBase.driver.findElement(By.xpath("(//*[contains(text(), 'Post Me A Brochure')])"+"[" +i+"]")).isDisplayed()) {
				Thread.sleep(2000);
				TestBase.driver.findElement(By.xpath("//*[contains(text(), 'Post Me A Brochure')]")).click();
			}
			break;
		}
		
		Thread.sleep(20000);
		/*
		 * String url=TestBase.driver.getCurrentUrl(); String url1 = url+
		 * Variables.ipaddress2; TestBase.driver.get(url1);
		 */
		TestBase.driver.get("https://qa2.aptouring.com.au/why-choose-apt/brochures/requestbrochure?brochureId=0116a129-54c4-4316-a5fd-d4806f4c3d66&ip=116.240.204.120");
		for (String handle : TestBase.driver.getWindowHandles()) {
			TestBase.driver.switchTo().window(handle);
			Thread.sleep(3000);
			if (Variables.REQUEST_BROCHURE.equalsIgnoreCase(TestBase.driver.getTitle().toString())) {
			//new Select (TestBase.driver.findElement(By.name("9080a0345570425292b7349253ac5cb7_title"))). selectByVisibleText(userDetails.get(0).get(0));
			TestBase.PerformActionOnElement("title_BrochureReq", "Type", userDetails.get(0).get(0));
				TestBase.PerformActionOnElement("fName_BrochureReq", "Type", userDetails.get(0).get(1));
				TestBase.PerformActionOnElement("lName_BrochureReq", "Type", userDetails.get(0).get(2));
				 TestBase.PerformActionOnElement("address_BrochureReq", "Type", userDetails.get(0).get(5));
			
				
				  if (!Variables.AUSTRALIA.equals(userDetails.get(0).get(3))) {
				  TestBase.PerformActionOnElement("country_BrochureReq", "Type",userDetails.get(0).get(3));
				  } else {
				  TestBase.PerformActionOnElement("state_BrochureReq", "Type",userDetails.get(0).get(4)); }
				 
				TestBase.PerformActionOnElement("citySuburb_BrochureReq", "Type", userDetails.get(0).get(6));
				TestBase.PerformActionOnElement("postCode_BrochureReq", "Type", userDetails.get(0).get(7));
				TestBase.PerformActionOnElement("phoneNumber_BrochureReq", "Type", userDetails.get(0).get(8));
				TestBase.PerformActionOnElement("emailAddress_BrochureReq", "Type", userDetails.get(0).get(9));
				TestBase.PerformActionOnElement("HowDidYouHearAbtUS_BrochureReq", "Type", "Mail");
				Thread.sleep(2000);
				TestBase.PerformActionOnElement("HowDidYouHearAbtUS_BrochureReq2", "Type", "Brochure");
				TestBase.driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
				Thread.sleep(3000);
				System.out.println(TestBase.driver.getCurrentUrl());
				TestBase.driver.findElement(By.xpath("//*[contains(text(), 'Thank you for your brochure request')]"));
				Thread.sleep(5000);
				break;
			}
	
		}
	}

}