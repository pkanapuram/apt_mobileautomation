package pages;

import java.util.List;
import java.util.function.Function;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.UtilityHelper;
import utils.Variables;
import base.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import utils.LogHelper;

public class TripBookingPage extends TestBase{
	private AppiumDriver<?> driver;
	public static Logger logger = LogHelper.getLogger(HomePage.class);
	private boolean singleRoomType = false;

	public TripBookingPage(AndroidDriver<?> driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	//public static Logger logger = LogHelper.getLogger(TripBookingPage.class);

	public void clickOnTrip() throws Exception{

		TestBase.driver.findElement(By.xpath("/html/body/div[3]/article[2]/section[1]/div/div/button")).click();
		logger.info("Proceeding with the trip ");
	}
	public void selectPrice_Availability(List<List<String>> dateList) throws InterruptedException {
		
		Thread.sleep(3000);
		List<WebElement> dates = (List<WebElement>) TestBase.driver.findElements(By.cssSelector(".trip-priceandbooking__tile-cell"));
		for (WebElement date : dates) {
			logger.debug("selecting date: " + date.getText());
			UtilityHelper.sleep();
			if (!(date.getText().contains("No Availability")) && date.getText().contains(dateList.get(0).get(0))
					&& date.getText().contains(dateList.get(0).get(1))
					&& date.getText().contains(dateList.get(0).get(2))) {
				logger.debug("selecting date: " + date.getText());
				date.click();
				break;
			}
		}
	}
	
	public void selectRoomType(String roomType, String cabinType) throws InterruptedException {
		UtilityHelper.sleep();
		List<WebElement> rooms = TestBase.driver
				.findElements(By.xpath(".//*[contains(@class, 'col-sm-4 col-xs-12 ng-scope ng-isolate-scope')]"));
		logger.debug("rooms " + rooms);
		for (WebElement room : rooms) {
			logger.debug("rooms " + room.getText());
			if (room.getText().contains(roomType)) {
				logger.debug("Selecting roomtype : " + roomType);
				room.click();
				if (Variables.ROOMTYPE_SINGLE.equalsIgnoreCase(roomType)) {
					singleRoomType = true;
				}
				break;
			}
		} // CABIN SELECTION
		if (!cabinType.equals("")) {
			List<WebElement> cabins = TestBase.driver.findElements(By.cssSelector("col-sm-4 col-xs-12 ng-scope"));
			for (WebElement cabin : cabins) {
				if (cabin.getText().equalsIgnoreCase(cabinType)) {
					logger.debug("Selecting cabintype : " + cabinType);
					cabin.click();
				}
			}
		} else {
			logger.debug("No Cabins to select");
		}
		Thread.sleep(5000);
		/*
		 * WebDriverWait wait = new WebDriverWait(driver,30); wait.until((Function<?
		 * super WebDriver, V>) ExpectedConditions.elementIfVisible((WebElement) By.
		 * xpath("//*[@class='btn btn--primary btn--large trip-priceandbooking__continue__btn']"
		 * )));
		 */
		
		TestBase.driver.findElement(By.xpath("//*[@class='btn btn--primary btn--large trip-priceandbooking__continue__btn']")).click();
		//TestBase.waitForElement(continueEquiry).click();
	}
	
	public void enterBookingDetails(List<List<String>> detailsList) {
		logger.info("Entering personal details to continue with the booking");
		if (singleRoomType) {
			TestBase.driver.findElement(By.name("e95f785b6c8d40649900876fb39e8e21_title")).sendKeys(detailsList.get(0).get(0).toString());
			TestBase.driver.findElement(By.name("ee0bc58389fb4f8e85c73a3e59cd9972_firstname")).sendKeys(detailsList.get(0).get(1).toString());
			TestBase.driver.findElement(By.name("20363691315c4bd6ac6fc5b41a66fcfd_lastname")).sendKeys(detailsList.get(0).get(2).toString());
			TestBase.driver.findElement(By.name("ee3c8d821054460a957569cda913422e_emailaddress")).sendKeys(detailsList.get(0).get(3).toString());
			TestBase.driver.findElement(By.name("c5878e29dd5646808777ac352eaf80dc_phonenumber")).sendKeys(detailsList.get(0).get(4).toString());
			TestBase.driver.findElement(By.name("submit")).click();
			TestBase.driver.findElement(By.xpath(UtilityHelper.containsString("Book now", "a"))).click();
		} else {// multiple passengers
			TestBase.driver.findElement(By.name("e95f785b6c8d40649900876fb39e8e21_title")).sendKeys(detailsList.get(0).get(0).toString());
			TestBase.driver.findElement(By.name("ee0bc58389fb4f8e85c73a3e59cd9972_firstname")).sendKeys(detailsList.get(0).get(1).toString());
			TestBase.driver.findElement(By.name("20363691315c4bd6ac6fc5b41a66fcfd_lastname")).sendKeys(detailsList.get(0).get(2).toString());
			TestBase.driver.findElement(By.name("ee3c8d821054460a957569cda913422e_emailaddress")).sendKeys(detailsList.get(0).get(3).toString());
			TestBase.driver.findElement(By.name("c5878e29dd5646808777ac352eaf80dc_phonenumber")).sendKeys(detailsList.get(0).get(4).toString());
			TestBase.driver.findElement(By.name("f8c4094d20ca4c9493d1dcd143925721_title")).sendKeys(detailsList.get(0).get(0).toString());
			TestBase.driver.findElement(By.name("16b9a41e31b74c39be5a3612ef7cb6c6_firstnamesecond")).sendKeys(detailsList.get(0).get(1).toString());
			TestBase.driver.findElement(By.name("fc0d0c1a2eb74da29f581ed573396b9a_lastnamesecond")).sendKeys(detailsList.get(0).get(2).toString());
		
		}
	}
}
