package pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import base.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import utils.LogHelper;

public class TMNewsletterPage {
	private AppiumDriver<?> driver;
	public static Logger logger = LogHelper.getLogger(BOTNewsletterPage.class);
	
	public TMNewsletterPage(AndroidDriver<?> driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void signUpNewsLetter(List<List<String>> userDetails) throws Exception {
		
		TestBase.PerformActionOnElement("title_TMNewLetter", "Type", userDetails.get(0).get(0));
		TestBase.PerformActionOnElement("fName_TMNewsletter", "Type", userDetails.get(0).get(1));
		TestBase.PerformActionOnElement("lName_TMNewsletter", "Type", userDetails.get(0).get(2));
		TestBase.PerformActionOnElement("emailAddress_TMNewsletter", "Type", userDetails.get(0).get(3));
		logger.info("User Details entered for Newsletter");
	}

	public void newsletterSignup() throws Exception {
		TestBase.PerformActionOnElement("submit_TMNewsletter", "Click", "");
		logger.info("Clicking on Submit");
		//signup_newsletter.click();
	}

	public String newsletterText() {
		return TestBase.driver.findElement(By.xpath("//h3[contains(text(), 'Thank you for signing up to our newsletter')]")).getText();
		//logger.info("Newsletter SIgned up Successfully");
	}

}
