package pages;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.google.common.collect.Iterables;
import base.TestBase;
import pages.HomePage;
import utils.UtilityHelper;
import utils.LogHelper;
import utils.UtilityHelper;
import utils.Variables;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class HomePage extends TestBase {
	
		// TODO Auto-generated constructor stub
	private AppiumDriver<?> driver;
	public static Logger logger = LogHelper.getLogger(HomePage.class);
	private boolean singleRoomType = false;
	
	

	
	  public HomePage(AndroidDriver<?> driver) { this.driver = driver;
	  PageFactory.initElements(this.driver, this); }
	 

	public void verifyPageTitle() {

		logger.info(TestBase.driver.findElement(By.xpath("/html/head/title")).getText());
	}

	
	public void searchByCode(String tripCode) throws Exception {
		try {
			TestBase.PerformActionOnElement("menu", "Click", "");
			TestBase.PerformActionOnElement("navSearch", "Type", tripCode);
			TestBase.PerformActionOnElement("zoomSearch", "Click", "");
			logger.info("Searching for :" + tripCode);
			System.out.println(tripCode);

			logger.info("Searching for :" + tripCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

	public void viewTrips() throws Exception {
		TestBase.PerformActionOnElement("viewTrips", "Click", "");
	}
	
	

	public void searchTrips(List<List<String>> searchList) throws Exception {
		
		  Thread.sleep(3000);
		  TestBase.driver.findElement(By.xpath("( //*[@id='trip-search-widget-mobile']/li/div/h6/a)[1]")).click();
		  Thread.sleep(3000);
		  
		  List<WebElement> listDestinations = (List<WebElement>) TestBase.driver.findElements(By.xpath(
					"/html/body/div[3]/section[2]/form/div[2]/data-trip-search-widget-panel[1]/ul/li/ul/li/a"));
		  logger.debug("Destinations " + listDestinations.size());
		  
		  for (WebElement listDestination : listDestinations) {
			  if(listDestination.getText().contains(searchList.get(0).get(0))) {
				  System.out.println(listDestination.getText());
				  listDestination.click();  
				  break;
			  }
		  }
		  
		  
		  Thread.sleep(3000);
		  TestBase.driver.findElement(By.xpath("(//*[@id='trip-search-widget-mobile']/li/div/h6/a)[2]")).click();
 Thread.sleep(3000);
		  
		  List<WebElement> listTravelStyles = (List<WebElement>) TestBase.driver.findElements(By.xpath(
					"/html/body/div[3]/section[2]/form/div[2]/data-trip-search-widget-panel[2]/ul/li/ul/li/a"));
		  logger.debug("TravelStyles " + listTravelStyles.size());
		  
		  for (WebElement listTravelStyle : listTravelStyles) {
			  if(listTravelStyle.getText().contains(searchList.get(0).get(1))) {
				  listTravelStyle.click();  
				  break;
			  }
		  }		
		  
		  
		  Thread.sleep(3000);
		  TestBase.driver.findElement(By.xpath("( //*[@id='trip-search-widget-mobile']/li/div/h6/a)[3]")).click();
 Thread.sleep(3000);
		  
		  List<WebElement> listDurations = (List<WebElement>) TestBase.driver.findElements(By.xpath(
					"/html/body/div[3]/section[2]/form/div[2]/data-trip-search-widget-panel[3]/ul/li/ul/li/a"));
		  logger.debug("Durations " + listDurations.size());
		  
		  for (WebElement listDuration : listDurations) {
			  if(listDuration.getText().contains(searchList.get(0).get(2))) {
				  listDuration.click();  
				  break;
			  }
		  }	
		  Thread.sleep(3000);
		  TestBase.driver.findElement(By.xpath("(//*[contains(text(),'Search trips')])[2]")).click();
		  Thread.sleep(5000);
	
		TestBase.PerformActionOnElement("filter", "Click", "");
		Thread.sleep(1000);
		TestBase.driver.findElement(By.xpath("//a[contains(text(), '2020')]")).click();
		Thread.sleep(3000);
		TestBase.driver.findElement(By.xpath("//*[@id='trip-finder-search-filter']/div/div/label")).click();
		Thread.sleep(3000);
		
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		String url=TestBase.driver.getCurrentUrl();
		 String url1 = url+ Variables.ipaddress2;
		 TestBase.driver.get(url1);
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		
		 List<WebElement> searchElements = TestBase.driver.findElements(By.xpath(
				".//*[contains(@class, 'col-xs-12 col-sm-4 trip-card-listing__cell trip-card-xs-small trip-card-sm-small ng-scope')]"));
		logger.debug("searchElements " + searchElements.size());
		for (WebElement searchElement : searchElements) {
			Actions a = new Actions(TestBase.driver);
			a.contextClick(searchElement).build().perform();
			Thread.sleep(3000);
			a.keyDown(Keys.CONTROL).sendKeys(Keys.HOME).click().build().perform();
		}
	}
	

	public void verifyTrips(List<List<String>> data) {
		Set<String> tabs = TestBase.driver.getWindowHandles();
		for (String tab : tabs) {
			TestBase.driver.switchTo().window(tab);
			if (TestBase.driver.getCurrentUrl().toString().contains(data.get(0).get(0)) && TestBase.driver.findElement(By.xpath("/html/body/div[3]/article[2]/section[1]/div/div/ul/li/a"))
					.getText().toString().contains(data.get(0).get(1))) {
			}
		}
		logger.debug("All trips for " + data.get(0).get(0) + " and " + data.get(0).get(1) + "are shown");
	}
	
	public void searchBySD() {
		try {
			TestBase.PerformActionOnElement("menu", "Click", "");
			TestBase.PerformActionOnElement("specialDeals", "Click", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

	public boolean selectsTripBySD(String region, String deal) {
		boolean tripfound = false;
		try {

			TestBase.driver.findElement(By.xpath(UtilityHelper.containsString(region, "button"))).click();
			UtilityHelper.sleep();
			TestBase.driver.findElement(By.xpath(UtilityHelper.containsString("All " + region + " Specials", "a")))
					.click();

			//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
			System.out.println(TestBase.driver.getCurrentUrl());
			String url = TestBase.driver.getCurrentUrl();
			String url1 = url + Variables.ipaddress;
			TestBase.driver.get(url1);
			//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------

			System.out.println(TestBase.driver
					.findElements(By.xpath(UtilityHelper.containsString("View Itineraries", "a"))).size());

			List<WebElement> titles = (List<WebElement>) TestBase.driver
					.findElements(By.xpath("//*[contains(text(), 'View Itineraries')]//preceding-sibling::h3"));
			int titleSize = titles.size();

			List<WebElement> itineraries = (List<WebElement>) TestBase.driver
					.findElements(By.xpath(UtilityHelper.containsString("View Itineraries", "a")));

			int itinerariesSize = itineraries.size();

			for (int i = 0; i <= titleSize; i++) {
				String title = titles.get(i).getText();
				System.out.println(title);
				if (titles.get(i).getText().equalsIgnoreCase(deal)) {
					itineraries.get(i).click();
					logger.debug("trip found");

					System.out.println(TestBase.driver.getCurrentUrl());
					
					//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
					String url2 = TestBase.driver.getCurrentUrl();
					String url3 = url2 + Variables.ipaddress;
					TestBase.driver.get(url3);
					System.out.println(TestBase.driver.getCurrentUrl());
					//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------

					List<WebElement> specialDealTrips = (List<WebElement>) TestBase.driver
							.findElements(By.xpath("//*[@id='trip-listing']/div/div/div/section/div/a[2]"));

					for (int k = 1; k <= itinerariesSize; k++) {

						List<WebElement> TRIPSandQUOTE = (List<WebElement>) TestBase.driver.findElements(By.xpath(
								"//*[@class = 'btn btn--primary trip-card__info'] | //*[@class = 'btn btn--primary trip-card__info ng-scope']"));

						List<WebElement> TRIPSandQUOTE_TITLES = (List<WebElement>) TestBase.driver
								.findElements(By.xpath("//*[@class = 'h4 trip-card__title'] "));

						if (TestBase.driver.findElement(By.xpath(
								"//*[@class = 'btn btn--primary trip-card__info'] | //*[@class = 'btn btn--primary trip-card__info ng-scope']"))
								.isDisplayed()) {
							for (int l = 0; l <= TRIPSandQUOTE.size(); l++) {

								System.out.println(TRIPSandQUOTE_TITLES.get(l).getText());
								logger.info("Trip's enabled for this Deal- " + TRIPSandQUOTE_TITLES.get(l).getText());
							}
						}

						else if ((TestBase.driver
								.findElement(
										By.xpath("//*[@id='trip-listing']/div/div/div[" + k + "]/section/div/a[2]"))
								.getText().contains(Variables.QUOTE))) {
							TestBase.driver
									.findElement(
											By.xpath("//*[@id='trip-listing']/div/div/div[" + k + "]/section/div/a[2]"))
									.click();
							tripfound = true;
							logger.info("Trip's enabled for this Deal");
							break;

						}

						else {
							logger.info("No Trip's enabled for this Deal");

						}
					}
				} else {
					break;
				}
				if (tripfound)
					break;
			}
		} catch (Exception e) {

		}
		return tripfound;
	}

	public String getSearchText() {
		return TestBase.driver.findElement(By.id("nav-search")).getText().toString();
	}
	
	
	
	
	
	// Booking Page code- to be deleted
	public void clickOnTrip() throws Exception{
		TestBase.driver.findElement(By.xpath("//*[@class='trip-intro__price']")).click();
		
	}
	public void selectPrice_Availability(List<List<String>> dateList) {
		
		UtilityHelper.sleep();
	
	
		List<WebElement> dates = (List<WebElement>) TestBase.driver.findElements(By.cssSelector(".trip-priceandbooking__tile-cell"));
		for (WebElement date : dates) {
			logger.debug("selecting date: " + date.getText());
			UtilityHelper.sleep();
			if (!(date.getText().contains("No Availability")) && date.getText().contains(dateList.get(0).get(0))
					&& date.getText().contains(dateList.get(0).get(1))
					&& date.getText().contains(dateList.get(0).get(2))) {
				logger.debug("selecting date: " + date.getText());
				date.click();
				break;
			}
		}
	}
	
	
	
	
	public void selectRoomType(String roomType, String cabinType) {
		UtilityHelper.sleep();
		List<WebElement> rooms = TestBase.driver
				.findElements(By.xpath(".//*[contains(@class, 'col-sm-4 col-xs-12 ng-scope ng-isolate-scope')]"));
		logger.debug("rooms " + rooms);
		for (WebElement room : rooms) {
			logger.debug("rooms " + room.getText());
			if (room.getText().contains(roomType)) {
				logger.debug("Selecting roomtype : " + roomType);
				room.click();
				if (Variables.ROOMTYPE_SINGLE.equalsIgnoreCase(roomType)) {
					singleRoomType = true;
				}
				break;
			}
		} // CABIN SELECTION
		if (!cabinType.equals("")) {
			List<WebElement> cabins = TestBase.driver.findElements(By.cssSelector("col-sm-4 col-xs-12 ng-scope"));
			for (WebElement cabin : cabins) {
				if (cabin.getText().equalsIgnoreCase(cabinType)) {
					logger.debug("Selecting cabintype : " + cabinType);
					cabin.click();
				}
			}
		} else {
			logger.debug("No Cabins to select");
		}
		UtilityHelper.sleep();
		TestBase.driver.findElement(By.xpath("//*[@class='btn btn--primary btn--large trip-priceandbooking__continue__btn']")).click();
		
	}
	
	public void enterBookingDetails(List<List<String>> detailsList) {
		logger.info("Entering personal details to continue with the booking");
		if (singleRoomType) {
			TestBase.driver.findElement(By.name("e95f785b6c8d40649900876fb39e8e21_title")).sendKeys(detailsList.get(0).get(0).toString());
			TestBase.driver.findElement(By.name("ee0bc58389fb4f8e85c73a3e59cd9972_firstname")).sendKeys(detailsList.get(0).get(1).toString());
			TestBase.driver.findElement(By.name("20363691315c4bd6ac6fc5b41a66fcfd_lastname")).sendKeys(detailsList.get(0).get(2).toString());
			TestBase.driver.findElement(By.name("ee3c8d821054460a957569cda913422e_emailaddress")).sendKeys(detailsList.get(0).get(3).toString());
			TestBase.driver.findElement(By.name("c5878e29dd5646808777ac352eaf80dc_phonenumber")).sendKeys(detailsList.get(0).get(4).toString());
			TestBase.driver.findElement(By.name("submit")).click();
			TestBase.driver.findElement(By.xpath(UtilityHelper.containsString("Book now", "a"))).click();
		} else {// multiple passengers
			TestBase.driver.findElement(By.name("e95f785b6c8d40649900876fb39e8e21_title")).sendKeys(detailsList.get(0).get(0).toString());
			TestBase.driver.findElement(By.name("ee0bc58389fb4f8e85c73a3e59cd9972_firstname")).sendKeys(detailsList.get(0).get(1).toString());
			TestBase.driver.findElement(By.name("20363691315c4bd6ac6fc5b41a66fcfd_lastname")).sendKeys(detailsList.get(0).get(2).toString());
			TestBase.driver.findElement(By.name("ee3c8d821054460a957569cda913422e_emailaddress")).sendKeys(detailsList.get(0).get(3).toString());
			TestBase.driver.findElement(By.name("c5878e29dd5646808777ac352eaf80dc_phonenumber")).sendKeys(detailsList.get(0).get(4).toString());
			TestBase.driver.findElement(By.name("f8c4094d20ca4c9493d1dcd143925721_title")).sendKeys(detailsList.get(0).get(0).toString());
			TestBase.driver.findElement(By.name("16b9a41e31b74c39be5a3612ef7cb6c6_firstnamesecond")).sendKeys(detailsList.get(0).get(1).toString());
			TestBase.driver.findElement(By.name("fc0d0c1a2eb74da29f581ed573396b9a_lastnamesecond")).sendKeys(detailsList.get(0).get(2).toString());
	
		}
	}
	
	
	
	
	
	
	// Payment Page code- to be deleted
	public boolean proceesPayment(List<List<String>> paymentDetailsList) throws InterruptedException {
		logger.info("Proceeding with the payment");
		if (paymentDetailsList.get(0).get(2).equalsIgnoreCase(Variables.FULL_AMOUNT)) {
			driver.findElement(By.xpath("//span[contains(text(), 'Pay Total Amount')]")).click();
		}
		if (paymentDetailsList.get(0).get(1).equalsIgnoreCase(Variables.DEBIT)) {
			TestBase.driver.findElement(By.xpath("//label[contains(text(), 'Debit')]")).click();
		} else {
			TestBase.driver.findElement(By.xpath(
					"//label[contains(text(), 'Credit')]")).click();
		}
		TestBase.driver.findElement(By.name("c0467154a690424f824bd126e709d8f5_cardholder name")).sendKeys(paymentDetailsList.get(0).get(2));
		Thread.sleep(3000);
		
		TestBase.driver.switchTo().frame(Variables.CARD_NUMBER_FRAME_NAME);
		TestBase.driver.findElement(By.name("credit-card-number")).sendKeys(paymentDetailsList.get(0).get(3));
		TestBase.driver.switchTo().defaultContent();
		TestBase.driver.switchTo().frame(Variables.EXPIRATION_FRAME_NAME);
		TestBase.driver.findElement(By.name("expiration")).sendKeys(paymentDetailsList.get(0).get(4));
		TestBase.driver.switchTo().defaultContent();
		TestBase.driver.switchTo().frame(Variables.CVV_FRAME_NAME);
		TestBase.driver.findElement(By.name("cvv")).sendKeys(paymentDetailsList.get(0).get(5));
		TestBase.driver.switchTo().defaultContent();
		((JavascriptExecutor) TestBase.driver)
				.executeScript("$('D2A819102BAD4F219CB634FA9ECE760C_opt-in-checkbox').removeClass('hide');");
		((JavascriptExecutor) TestBase.driver)
				.executeScript("document.getElementById('D2A819102BAD4F219CB634FA9ECE760C_opt-in-checkbox').click();");
		TestBase.driver.findElement(By.xpath("//button[contains(text(), 'Process Payment')]")).click();
		return TestBase.driver.findElement(By.cssSelector(".booking-form__success__what-next")).isDisplayed();
	}
	
	
	
	//NewsletterPage
	public void signUpNewsLetter(List<List<String>> userDetails) {
		TestBase.driver.findElement(By.name("d36b412f86124675badc508a5a542418_title")).sendKeys(userDetails.get(0).get(0));
		TestBase.driver.findElement(By.name("3d93ae1c8955447c92d00d5ae7e997ba_firstname")).sendKeys(userDetails.get(0).get(1));
		TestBase.driver.findElement(By.name("9aa2da40fa274abda635156bc36d24a5_lastname")).sendKeys(userDetails.get(0).get(2));
		TestBase.driver.findElement(By.name("a0c82094abc845adb79661f1df522c61_emailaddress")).sendKeys(userDetails.get(0).get(3));
	}
	
	public void newsletterSignup() {
		TestBase.driver.findElement(By.name("submit")).click();
	}
	
	public String newsletterText() {
		return TestBase.driver.findElement(By.xpath("//h3[contains(text(), 'THANK YOU FOR SIGNING UP TO OUR NEWSLETTER')]")).getText();
	}
	
	
	
	
	//ContactUSPage
	public void contactUs() {
		TestBase.driver.findElement(By.xpath(
				"(//*[@class='nav-heading-link nav-title-link collapsed' ])[5] | (//*[@class='nav-heading-link nav-title-link'])[5]"))
				.click();
		TestBase.driver.findElement(By.xpath("//h5/a[@href='/about-us/contact-us' and contains (text(),'Contact Us')]")).click();
		
	}

	public void topic(String topicId) {
		
		TestBase.driver.findElement(By.name("2ce6aed478f643fcacf5ac1787aca990_selectedtopicid")).sendKeys(topicId);
	}

	public void sendMessage(String msg) {
		TestBase.driver.findElement(By.xpath("//*[@id='e0ae8556429e4d05a6b5656700170e26_comments']")).sendKeys(msg);
		
	}

	public String success() {
		return TestBase.driver.findElement(By.xpath("//h2[contains(text(), 'Thank you')]")).getText();
	}

	public void sendEnquiryDetails(List<List<String>> userDetails) {
		//UtiltyHelper.sleep();
		TestBase.driver.findElement(By.name("15c3573416c543a596e2510c271bca4b_title")).sendKeys(userDetails.get(0).get(0));
		TestBase.driver.findElement(By.name("a3d85e947986449b99a2f341b84b5af0_firstname")).sendKeys(userDetails.get(0).get(1));
		TestBase.driver.findElement(By.name("3e7303847592409fa46be7858f5abaee_lastname")).sendKeys(userDetails.get(0).get(2));
		if (!Variables.INDIA.equals(userDetails.get(0).get(3))) {
			TestBase.driver.findElement(By.name("f320c107bf28497a86d019d954e9aae2_country")).sendKeys(userDetails.get(0).get(3));
		} else {
			TestBase.driver.findElement(By.name("89330848dfac407e87328f4473bf489b_statecountryprovince")).sendKeys(userDetails.get(0).get(5));
		}
		TestBase.driver.findElement(By.name("65429c8096d443c3aa65a32a9b0990f3_postcode")).sendKeys(userDetails.get(0).get(4));
		TestBase.driver.findElement(By.name("9252412c7d1e468c94d7660e71f8283f_phonenumber")).sendKeys(userDetails.get(0).get(6));
		TestBase.driver.findElement(By.name("57dea9488771491d8fe72fadcaabd668_emailaddress")).sendKeys(userDetails.get(0).get(7));
		TestBase.driver.findElement(By.xpath("//*[contains(text(), 'Send Your Enquiry')]")).click();
	}


}


