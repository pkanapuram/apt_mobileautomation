package pages;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import base.TestBase;
import io.appium.java_client.android.AndroidDriver;
import utils.LogHelper;
import utils.Variables;

public class TMSearchFilterPage extends TestBase {
	public static Logger logger = LogHelper.getLogger(TMSearchFilterPage.class);
	private WebDriver driver;

	public TMSearchFilterPage(AndroidDriver<?> driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void searchTrip(List<List<String>> searchList) throws Exception {

		Thread.sleep(3000);
		TestBase.driver.findElement(By.xpath("( //*[@id='trip-search-widget-mobile']/li/div/h6/a)[1]")).click();
		Thread.sleep(3000);
		List<WebElement> listDestinations = (List<WebElement>) TestBase.driver.findElements(
				By.xpath("/html/body/div[2]/section[2]/form/div[2]/data-trip-search-widget-panel[1]/ul/li/ul/li"));
		logger.debug("Destinations " + listDestinations.size());
		for (WebElement listDestination : listDestinations) {
			if (listDestination.getText().contains(searchList.get(0).get(0))) {
				System.out.println(listDestination.getText());
				listDestination.click();
				break;
			}
		}

		Thread.sleep(3000);
		TestBase.driver.findElement(By.xpath("(//*[@id='trip-search-widget-mobile']/li/div/h6/a)[2]")).click();
		Thread.sleep(3000);

		List<WebElement> listTravelStyles = (List<WebElement>) TestBase.driver.findElements(
				By.xpath("/html/body/div[2]/section[2]/form/div[2]/data-trip-search-widget-panel[2]/ul/li/ul/li"));
		logger.debug("TravelStyles " + listTravelStyles.size());

		for (WebElement listTravelStyle : listTravelStyles) {
			if (listTravelStyle.getText().contains(searchList.get(0).get(1))) {
				listTravelStyle.click();
				break;
			}
		}

		Thread.sleep(3000);
		TestBase.driver.findElement(By.xpath("( //*[@id='trip-search-widget-mobile']/li/div/h6/a)[3]")).click();
		Thread.sleep(3000);

		List<WebElement> listDurations = (List<WebElement>) TestBase.driver.findElements(
				By.xpath("/html/body/div[2]/section[2]/form/div[2]/data-trip-search-widget-panel[3]/ul/li/ul/li"));
		logger.debug("Durations " + listDurations.size());

		for (WebElement listDuration : listDurations) {
			if (listDuration.getText().contains(searchList.get(0).get(2))) {
				listDuration.click();
				break;
			}
		}
		Thread.sleep(3000);
		TestBase.driver.findElement(By.xpath("(//*[contains(text(),'Search trips')])[2]")).click();
		Thread.sleep(5000);

		
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		String url = TestBase.driver.getCurrentUrl();
		TestBase.driver.get(url + Variables.ipaddress2);
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		
		
		Thread.sleep(3000);
		List<WebElement> searchElements = TestBase.driver.findElements(By.xpath(
				".//*[contains(@class, 'col-xs-12 col-sm-4 trip-card-listing__cell trip-card-xs-small trip-card-sm-small ng-scope')]"));
		logger.debug("searchElements " + searchElements.size());
		 for (WebElement searchElement : searchElements) {
		//for (int i = 1; i <= searchElements.size(); i++) {
			Actions a = new Actions(TestBase.driver);
			 a.contextClick(searchElement).build().perform();
			Thread.sleep(3000);
			a.keyDown(Keys.CONTROL).sendKeys(Keys.HOME).click().build().perform();
			// break;
		}

	}

	public void verifyTrip(List<List<String>> data) {
		Set<String> tabs = TestBase.driver.getWindowHandles();
		for (String tab : tabs) {
			TestBase.driver.switchTo().window(tab);
			if (TestBase.driver.getCurrentUrl().toString().contains(data.get(0).get(0)))
				//&& TestBase.driver.findElement(By.xpath("/html/body/div[2]/article[3]/div[1]/div[2]/div/section/div[1]/a/h1")).getText().toString().contains(data.get(0).get(1))) 
{
			}
		}
		logger.debug("All trips for " + data.get(0).get(0) + " and " + data.get(0).get(1) + "are shown");
	}

}
