package pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.PageFactory;

import base.TestBase;
import utils.Variables;
import utils.UtilityHelper;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import utils.LogHelper;

public class BOTContactUsPage {
	Logger log = LogHelper.getLogger(this.getClass());
	 private AppiumDriver<?> driver;
	ContactUsPage contactUSPage= new ContactUsPage(null);

	

	public BOTContactUsPage(AndroidDriver<?> driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void contactUs() throws InterruptedException {
		
		  JavascriptExecutor js = (JavascriptExecutor) TestBase.driver;
		  js.executeScript("window.scrollBy(0,3000)");

		TestBase.driver.findElement(By.xpath("//*[contains(text(),'Need help?')]")).click();
		Thread.sleep(3000);
		TestBase.driver.findElement(By.xpath("//a[@class='nav-title-link nav-heading-link' and contains(text(),'Contact Us')]")).click();
	}

	public void topic(String topicId) throws Exception {
		Thread.sleep(3000);
		TestBase.PerformActionOnElement("topicID_BOTContactUS", "Type", topicId);
		
	}

	public void sendMessage(String msg) throws Exception {
		TestBase.PerformActionOnElement("comments_BOTContactUs", "Type", msg);
		Thread.sleep(3000);
	}

	public String success() {
		UtilityHelper.sleep();
		log.info(TestBase.driver.findElement(By.xpath("//h2[contains(text(), 'Thank you')]")).getText());
		return TestBase.driver.findElement(By.xpath("//h2[contains(text(), 'Thank you')]")).getText();
	}

	public void sendEnquiryDetails(List<List<String>> userDetails) throws Exception {
		TestBase.PerformActionOnElement("title_BOTContactUs", "Type", userDetails.get(0).get(0));
		TestBase.PerformActionOnElement("fname_BOTContactUs", "Type", userDetails.get(0).get(1));
		TestBase.PerformActionOnElement("lname_BOTContactUs", "Type", userDetails.get(0).get(2));
		if (!Variables.AUSTRALIA.equals(userDetails.get(0).get(3))) {
			TestBase.PerformActionOnElement("country_BOTContactUs", "Type", userDetails.get(0).get(3));
			
		} else {
			TestBase.PerformActionOnElement("state_BOTContactUs", "Type", userDetails.get(0).get(5));
			
		}
		TestBase.PerformActionOnElement("postCode_BOTContactUs", "Type", userDetails.get(0).get(4));
		TestBase.PerformActionOnElement("phoneNumber_BOTContactUs", "Type", userDetails.get(0).get(6));
		TestBase.PerformActionOnElement("email_BOTContactUs", "Type", userDetails.get(0).get(7));
		TestBase.driver.findElement(By.xpath("//*[contains(text(), 'Send Your Enquiry')]")).click();
		
	}
	}

