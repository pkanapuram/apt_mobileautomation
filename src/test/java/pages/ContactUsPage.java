package pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import base.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import utils.LogHelper;
import utils.Variables;

public class ContactUsPage extends TestBase {
	
	  private AppiumDriver<?> driver;
	  public static Logger logger = LogHelper.getLogger(HomePage.class);
		private boolean singleRoomType = false;

	public ContactUsPage(AndroidDriver<?> driver) { this.driver = driver;
	  PageFactory.initElements(this.driver, this); }
	
	public void contactUs() {
		TestBase.driver.findElement(By.xpath(
				"(//*[@class='nav-heading-link nav-title-link collapsed' ])[5] | (//*[@class='nav-heading-link nav-title-link'])[5]"))
				.click();
		TestBase.driver.findElement(By.xpath("//h5/a[@href='/about-us/contact-us' and contains (text(),'Contact Us')]")).click();
		
	}

	public void topic(String topicId) throws Exception {
		Thread.sleep(3000);
		TestBase.PerformActionOnElement("topicID_ContactUS", "Type", topicId);
	}

	public void sendMessage(String msg) throws Exception {
		
		TestBase.PerformActionOnElement("comments_ContactUs", "Type", msg);
		Thread.sleep(3000);
	}

	public String success() {
		return TestBase.driver.findElement(By.xpath("//h2[contains(text(), 'Thank you')]")).getText();
	}

	public void sendEnquiryDetails(List<List<String>> userDetails) throws Exception {
	
		TestBase.PerformActionOnElement("title_ContactUs", "Type", userDetails.get(0).get(0));
		TestBase.PerformActionOnElement("fname_ContactUs", "Type", userDetails.get(0).get(1));
		TestBase.PerformActionOnElement("lname_ContactUs", "Type", userDetails.get(0).get(2));
		
		if (!Variables.AUSTRALIA.equals(userDetails.get(0).get(3))) {
			TestBase.PerformActionOnElement("country_ContactUs", "Type", userDetails.get(0).get(3));
			
		} else {
			TestBase.PerformActionOnElement("state_ContactUs", "Type", userDetails.get(0).get(5));
			
		}
		TestBase.PerformActionOnElement("postCode_ContactUs", "Type", userDetails.get(0).get(4));
		TestBase.PerformActionOnElement("phoneNumber_ContactUs", "Type", userDetails.get(0).get(6));
		TestBase.PerformActionOnElement("email_ContactUs", "Type", userDetails.get(0).get(7));
		TestBase.driver.findElement(By.xpath("//*[contains(text(), 'Send Your Enquiry')]")).click();
		
	}

}
