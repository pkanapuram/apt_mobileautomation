package pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import base.TestBase;
import pages.TMBrochurePage;
import utils.Variables;
import utils.LogHelper;
import utils.UtilityHelper;

public class TMBrochurePage {
	private WebDriver driver;

	Logger logger = LogHelper.getLogger(TMBrochurePage.class);

	public TMBrochurePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void searchByCode(String tripCode) {
		try {
			TestBase.PerformActionOnElement("menu", "Click", "");
			TestBase.PerformActionOnElement("navSearch", "Type", tripCode);
			TestBase.PerformActionOnElement("TMSearch", "Click", "");
			Thread.sleep(3000);
			logger.info("Searching for :" + tripCode);
			System.out.println(tripCode);

			logger.info("Searching for :" + tripCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void filterAndSelectBrochure() throws Exception {
		System.out.println(TestBase.driver.getCurrentUrl());
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		String url = TestBase.driver.getCurrentUrl();
		String url1 = url + Variables.ipaddress2;
		TestBase.driver.get(url1);
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		Thread.sleep(3000);
		TestBase.PerformActionOnElement("filters_TMBrochure", "Click", "");
		TestBase.PerformActionOnElement("selectFilter_TMBrochure", "Click", "");
		Thread.sleep(3000);
		TestBase.PerformActionOnElement("result_TMBrochure", "Click", "");
		Thread.sleep(3000);
	}

	public void enterDetails(List<List<String>> userDetails) throws Exception {

		List<WebElement> brochureReqs = TestBase.driver
				.findElements(By.xpath(".//*[contains(text(), 'Post Me A Brochure')]"));
		logger.debug("brochureReqs " + brochureReqs.size());
		for (int i = 1; i <= brochureReqs.size(); i++) {
			if (TestBase.driver.findElement(By.xpath("(//*[contains(text(), 'Post Me A Brochure')])" + "[" + i + "]"))
					.isDisplayed()) {
				Thread.sleep(2000);
				TestBase.driver.findElement(By.xpath("//*[contains(text(), 'Post Me A Brochure')]")).click();
			}
			break;
		}
		TestBase.driver.get(
				"https://qa2.travelmarvel.com.au/why-choose-travelmarvel/brochures/requestbrochure?brochureId=6abbe711-c380-4e80-87d3-62831260b002&ip=116.240.204.120");
		Thread.sleep(3000);

		for (String handle : TestBase.driver.getWindowHandles()) {
			TestBase.driver.switchTo().window(handle);
			if (Variables.TM_REQUEST_BROCHURE.equalsIgnoreCase(TestBase.driver.getTitle().toString())) {
				TestBase.PerformActionOnElement("title_TMBrochureReq", "Type", userDetails.get(0).get(0));
				TestBase.PerformActionOnElement("fName_TMBrochureReq", "Type", userDetails.get(0).get(1));
				TestBase.PerformActionOnElement("lName_TMBrochureReq", "Type", userDetails.get(0).get(2));
				TestBase.PerformActionOnElement("address_TMBrochureReq", "Type", userDetails.get(0).get(5));
				if (!Variables.AUSTRALIA.equalsIgnoreCase(userDetails.get(0).get(3))) {

					TestBase.PerformActionOnElement("country_TMBrochureReq", "Type", userDetails.get(0).get(3));
				} else {

					TestBase.PerformActionOnElement("state_TMBrochureReq", "Type", userDetails.get(0).get(4));
				}

				TestBase.PerformActionOnElement("citySuburb_TMBrochureReq", "Type", userDetails.get(0).get(6));
				TestBase.PerformActionOnElement("postCode_TMBrochureReq", "Type", userDetails.get(0).get(7));
				TestBase.PerformActionOnElement("phoneNumber_TMBrochureReq", "Type", userDetails.get(0).get(8));
				TestBase.PerformActionOnElement("emailAddress_TMBrochureReq", "Type", userDetails.get(0).get(9));
				UtilityHelper.sleep();
				TestBase.PerformActionOnElement("HowDidYouHearAbtUS_TMBrochureReq", "Type", "Mail");
				// TestBase.PerformActionOnElement("HowDidYouHearAbtUS_BrochureReq2", "Type",
				// "Family & Friends");
				TestBase.driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
				System.out.println(TestBase.driver.getCurrentUrl());
				TestBase.driver.findElement(By.xpath("//*[contains(text(), 'Thank you for your brochure request')]"));
				Thread.sleep(5000);
				break;
			}
		}
	}

	public boolean quoteSent() {
		System.out.println("success");
		return true;
	}
}