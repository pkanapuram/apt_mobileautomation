package pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import utils.Variables;
import utils.UtilityHelper;

import base.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import utils.LogHelper;

public class BOTBrochurePage extends TestBase {
	private AppiumDriver<?> driver;
	public static Logger logger = LogHelper.getLogger(BOTBrochurePage.class);

	public BOTBrochurePage(AndroidDriver<?> driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void searchByCode(String tripCode) throws Exception {
		try {
			TestBase.PerformActionOnElement("menu", "Click", "");
			TestBase.PerformActionOnElement("navSearch", "Type", tripCode);
			TestBase.PerformActionOnElement("botSearch", "Click", "");
			Thread.sleep(3000);
			logger.info("Searching for :" + tripCode);
			System.out.println(tripCode);

			logger.info("Searching for :" + tripCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void filterAndSelectBoucher() throws Exception {
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		System.out.println(TestBase.driver.getCurrentUrl());
		String url = TestBase.driver.getCurrentUrl();
		String url1 = url + Variables.ipaddress2;
		TestBase.driver.get(url1);
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		Thread.sleep(3000);
		// TestBase.driver.get("https://qa2.botanica.travel/search-results?search=botanica&ip=116.240.204.120");
		TestBase.PerformActionOnElement("filters_BotBrochure", "Click", ""); //Null ppointer exception
		TestBase.PerformActionOnElement("selectFilter_BotBrochure", "Click", "");
		Thread.sleep(3000);
		TestBase.PerformActionOnElement("result_BotBrochure", "Click", "");
		Thread.sleep(3000);
	}

	public boolean quoteSent() throws InterruptedException {
		System.out.println("success");
		return true;

	}

	public void enterDetailsForBroucherQuote(List<List<String>> userDetails) throws Exception {
		TestBase.PerformActionOnElement("postMeABrochureBOT", "Click", "");
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		System.out.println(TestBase.driver.getCurrentUrl());
		String url = TestBase.driver.getCurrentUrl();
		String url1 = url + Variables.ipaddress2;
		TestBase.driver.get(
				"https://qa2.botanica.travel/your-experience/brochures/requestbrochure?brochureId=75c8f095-d24a-4e5c-b48f-6b36c17696b4&ip=116.240.204.120");
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		Thread.sleep(3000);

		for (String handle : TestBase.driver.getWindowHandles()) {
			TestBase.driver.switchTo().window(handle);
			if (Variables.BOTANICA_REQUEST_BROCHURE.equalsIgnoreCase(TestBase.driver.getTitle().toString())) {
				TestBase.PerformActionOnElement("title_BOTBrochureReq", "Type", userDetails.get(0).get(0));
				TestBase.PerformActionOnElement("fName_BOTBrochureReq", "Type", userDetails.get(0).get(1));
				TestBase.PerformActionOnElement("lName_BOTBrochureReq", "Type", userDetails.get(0).get(2));
				TestBase.PerformActionOnElement("address_BOTBrochureReq", "Type", userDetails.get(0).get(5));
				if (!Variables.AUSTRALIA.equalsIgnoreCase(userDetails.get(0).get(3))) {

					TestBase.PerformActionOnElement("country_BOTBrochureReq", "Type", userDetails.get(0).get(3));
				} else {

					TestBase.PerformActionOnElement("state_BOTBrochureReq", "Type", userDetails.get(0).get(4));
				}

				TestBase.PerformActionOnElement("citySuburb_BOTBrochureReq", "Type", userDetails.get(0).get(6));
				TestBase.PerformActionOnElement("postCode_BOTBrochureReq", "Type", userDetails.get(0).get(7));
				TestBase.PerformActionOnElement("phoneNumber_BOTBrochureReq", "Type", userDetails.get(0).get(8));
				TestBase.PerformActionOnElement("emailAddress_BOTBrochureReq", "Type", userDetails.get(0).get(9));
				UtilityHelper.sleep();
				TestBase.PerformActionOnElement("HowDidYouHearAbtUS_BOTBrochureReq", "Type", "Mail");
				// TestBase.PerformActionOnElement("HowDidYouHearAbtUS_BrochureReq2", "Type",
				// "Family & Friends");
				TestBase.driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
				System.out.println(TestBase.driver.getCurrentUrl());
				TestBase.driver.findElement(By.xpath("//*[contains(text(), 'Thank you for your brochure request')]"));
				Thread.sleep(5000);
				break;
			}
		}
	}
}
