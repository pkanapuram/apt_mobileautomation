package pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utils.Variables;
import utils.UtilityHelper;
import base.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import utils.LogHelper;

public class BOTBookingPage extends TestBase {
	Logger logger = LogHelper.getLogger(this.getClass());
	 private AppiumDriver<?> driver;
	 private boolean singleRoomType = false;
	ContactUsPage contactUSPage= new ContactUsPage(null);

	

	public BOTBookingPage(AndroidDriver<?> driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	

	public void searchByCode(String tripCode) {
		try {
			TestBase.PerformActionOnElement("menu", "Click", "");
			TestBase.PerformActionOnElement("navSearch", "Type", tripCode);
			TestBase.PerformActionOnElement("botSearch", "Click", "");
			Thread.sleep(3000);
			logger.info("Searching for :" + tripCode);
			System.out.println(tripCode);

			logger.info("Searching for :" + tripCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
	} 
	
	public void clickOnTrip() {
		UtilityHelper.sleep();
		TestBase.driver.findElement(By.xpath("//*[@class='btn btn--primary btn--large trip-info ng-isolate-scope']")).click(); 
		logger.info("Proceeding with the trip ");
	}

	public void selectPrice_Availability(List<List<String>> dateList) {
		UtilityHelper.sleep();
		List<WebElement> dates = (List<WebElement>) TestBase.driver.findElements(By.cssSelector(".trip-priceandbooking__tile-cell"));
		for (WebElement date : dates) {
			UtilityHelper.sleep();
			if (!(date.getText().contains("No Availability")) && date.getText().contains(dateList.get(0).get(0))
					&& date.getText().contains(dateList.get(0).get(1))
					&& date.getText().contains(dateList.get(0).get(2))) {
				logger.debug("selecting date: " + date);
				date.click();
				break;
			}
		}
	}

	public void selectRoomType(String roomType) throws Exception {
		List<WebElement> rooms = (List<WebElement>) TestBase.driver
				.findElements(By.xpath(".//*[contains(@class, 'col-sm-4 col-xs-12 ng-scope ng-isolate-scope')]"));
		for (WebElement room : rooms) {
			if (room.getText().equalsIgnoreCase(roomType)) {
				logger.debug("Selecting roomtype : " + room);
				room.click();
				if (Variables.ROOMTYPE_SINGLE.equalsIgnoreCase(roomType)) {
					singleRoomType = true;
				}
				break;
			}
		}
		//TestBase.waitForElement(continueEquiry).click();
		TestBase.PerformActionOnElement("continueEnquiry", "Click", "");
	}

	public void enterDetails(List<List<String>> detailsList) {
		logger.info("Entering personal details to continue with the booking");
		if (singleRoomType) {
			TestBase.driver.findElement(By.name("625de9cf38ad44b1b019facbbe00aca9_title")).sendKeys(detailsList.get(0).get(0).toString());
			TestBase.driver.findElement(By.name("180abd5794d94298beaaedf886c0c57e_firstname")).sendKeys(detailsList.get(0).get(1).toString());
			TestBase.driver.findElement(By.name("b1ba40db35d042a292c680b47dad29ba_lastname")).sendKeys(detailsList.get(0).get(2).toString());
			TestBase.driver.findElement(By.name("00d11ead8eeb403c97d628b1822ebf46_emailaddress")).sendKeys(detailsList.get(0).get(3).toString());
			TestBase.driver.findElement(By.name("69c74ee530cd4ad7b1261d547963fab0_phonenumber")).sendKeys(detailsList.get(0).get(4).toString());
			TestBase.driver.findElement(By.name("submit")).click();
			TestBase.driver.findElement(By.xpath(UtilityHelper.containsString("Book now", "a"))).click();
		} else {// multiple passengers
			TestBase.driver.findElement(By.name("625de9cf38ad44b1b019facbbe00aca9_title")).sendKeys(detailsList.get(0).get(0).toString());
			TestBase.driver.findElement(By.name("180abd5794d94298beaaedf886c0c57e_firstname")).sendKeys(detailsList.get(0).get(1).toString());
			TestBase.driver.findElement(By.name("b1ba40db35d042a292c680b47dad29ba_lastname")).sendKeys(detailsList.get(0).get(2).toString());
			TestBase.driver.findElement(By.name("00d11ead8eeb403c97d628b1822ebf46_emailaddress")).sendKeys(detailsList.get(0).get(3).toString());
			TestBase.driver.findElement(By.name("69c74ee530cd4ad7b1261d547963fab0_phonenumber")).sendKeys(detailsList.get(0).get(4).toString());
			TestBase.driver.findElement(By.name("4f0e08c0cab4478eb7eecb950d608698_title")).sendKeys(detailsList.get(0).get(0).toString());
			TestBase.driver.findElement(By.name("780b8be87cb8433dae0fb9aa9442f902_firstnamesecond")).sendKeys(detailsList.get(0).get(1).toString());
			TestBase.driver.findElement(By.name("3d3bcbfb7df44f0db199025139edbb52_lastnamesecond")).sendKeys(detailsList.get(0).get(2).toString());
	}
	}
	

	public boolean proceesPayment(List<List<String>> paymentDetailsList) throws Exception {
		logger.info("Proceeding with the payment");
		if (paymentDetailsList.get(0).get(2).equalsIgnoreCase(Variables.FULL_AMOUNT)) {
			driver.findElement(By.xpath("//span[contains(text(), 'Pay total amount')]")).click();
		}
		if (paymentDetailsList.get(0).get(1).equalsIgnoreCase(Variables.DEBIT)) {
			//TestBase.waitForElement(debit).click();
			TestBase.PerformActionOnElement("debit", "Click", "");
		} else {
			TestBase.PerformActionOnElement("credit", "Click", "");
			//TestBase.waitForElement(credit).click();
		}
		
		TestBase.driver.findElement(By.name("a94656198e0442f9b9e3006639c97bb0_cardholder name")).sendKeys(paymentDetailsList.get(0).get(2));
		Thread.sleep(3000);
		//TestBase.driver.switchTo().frame("braintree-hosted-field-number");
		TestBase.driver.switchTo().frame(Variables.CARD_NUMBER_FRAME_NAME);
		TestBase.driver.findElement(By.name("credit-card-number")).sendKeys(paymentDetailsList.get(0).get(3));
		TestBase.driver.switchTo().defaultContent();
		TestBase.driver.switchTo().frame(Variables.EXPIRATION_FRAME_NAME);
		TestBase.driver.findElement(By.name("expiration")).sendKeys(paymentDetailsList.get(0).get(4));
		TestBase.driver.switchTo().defaultContent();
		TestBase.driver.switchTo().frame(Variables.CVV_FRAME_NAME);
		TestBase.driver.findElement(By.name("cvv")).sendKeys(paymentDetailsList.get(0).get(5));
		TestBase.driver.switchTo().defaultContent();
		((JavascriptExecutor) TestBase.driver)
				.executeScript("$('EFE15AEB748A4AB692CEA6502468A375_opt-in-checkbox').removeClass('hide');");
		((JavascriptExecutor) TestBase.driver)
				.executeScript("document.getElementById('EFE15AEB748A4AB692CEA6502468A375_opt-in-checkbox').click();");
		TestBase.driver.findElement(By.xpath("//button[contains(text(), 'Process Payment')]")).click();
		//return TestBase.driver.findElement(By.cssSelector(".booking-form__success__what-next")).isDisplayed();
		Thread.sleep(15000);
		return TestBase.driver.findElement(By.cssSelector(".booking-form__success__what-next")).isDisplayed();
	}

}
