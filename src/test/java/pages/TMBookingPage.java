package pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import base.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import utils.LogHelper;
import utils.UtilityHelper;
import utils.Variables;

public class TMBookingPage extends TestBase {
	Logger logger = LogHelper.getLogger(this.getClass());
	 private AppiumDriver<?> driver;
	 private boolean singleRoomType = false;
	ContactUsPage contactUSPage= new ContactUsPage(null);

	

	public TMBookingPage(AndroidDriver<?> driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	

	public void searchByCode(String tripCode) {
		try {
			TestBase.PerformActionOnElement("menu", "Click", "");
			TestBase.PerformActionOnElement("navSearch", "Type", tripCode);
			TestBase.PerformActionOnElement("TMSearch", "Click", "");
			Thread.sleep(3000);
			logger.info("Searching for :" + tripCode);
			System.out.println(tripCode);

			logger.info("Searching for :" + tripCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
	} 
	
	public void clickOnTrip() {
		UtilityHelper.sleep();
		TestBase.driver.findElement(By.xpath("/html/body/div[2]/main/div[1]/div/div[1]/button")).click();
		logger.info("Proceeding with the trip ");
	}

	public void selectPrice_Availability(List<List<String>> dateList) {
		UtilityHelper.sleep();
		List<WebElement> dates = (List<WebElement>) TestBase.driver.findElements(By.cssSelector(".trip-priceandbooking__tile-cell"));
		for (WebElement date : dates) {
			UtilityHelper.sleep();
			if (!(date.getText().contains("No Availability")) && date.getText().contains(dateList.get(0).get(0))
					&& date.getText().contains(dateList.get(0).get(1))
					&& date.getText().contains(dateList.get(0).get(2))) {
				logger.debug("selecting date: " + date);
				date.click();
				break;
			}
		}
	}

	public void selectRoomType(String roomType) throws Exception {
		List<WebElement> rooms = (List<WebElement>) TestBase.driver
				.findElements(By.xpath(".//*[contains(@class, 'col-sm-4 col-xs-12 ng-scope ng-isolate-scope')]"));
		for (WebElement room : rooms) {
			if (room.getText().equalsIgnoreCase(roomType)) {
				logger.debug("Selecting roomtype : " + room);
				room.click();
				if (Variables.ROOMTYPE_SINGLE.equalsIgnoreCase(roomType)) {
					singleRoomType = true;
				}
				break;
			}
		}
		//TestBase.waitForElement(continueEquiry).click();
		TestBase.PerformActionOnElement("continueEnquiry", "Click", "");
		Thread.sleep(5000);
	}

	public void enterDetails(List<List<String>> detailsList) throws Exception {
		logger.info("Entering personal details to continue with the booking");
		Thread.sleep(5000);
		if (singleRoomType) {
			
			TestBase.PerformActionOnElement("TMFirstTitle", "Type", detailsList.get(0).get(0).toString());
			TestBase.PerformActionOnElement("TMFirstName", "Type", detailsList.get(0).get(1).toString());
			TestBase.PerformActionOnElement("TMLastName", "Type", detailsList.get(0).get(2).toString());
			TestBase.PerformActionOnElement("TMEmailAddress", "Type", detailsList.get(0).get(3).toString());
			TestBase.PerformActionOnElement("TMPhoneNumber", "Type", detailsList.get(0).get(4).toString());
			/*
			 * TestBase.driver.findElement(By.name("81c9ee2a97194f50b91bbc5407d81da2_title")
			 * ).sendKeys(detailsList.get(0).get(0).toString());
			 * TestBase.driver.findElement(By.name(
			 * "e1b18cbfc6f8467a8312bf3f7dc40cc5_firstname")).sendKeys(detailsList.get(0).
			 * get(1).toString()); TestBase.driver.findElement(By.name(
			 * "5b8c768b09e24ce394f0549cec0479d4_lastname")).sendKeys(detailsList.get(0).get
			 * (2).toString()); TestBase.driver.findElement(By.name(
			 * "2e32a6f93bb84b0aac3de33f85eeac86_emailaddress")).sendKeys(detailsList.get(0)
			 * .get(3).toString()); TestBase.driver.findElement(By.name(
			 * "7897d5b79abf4028b6ccdafba951a310_phonenumber")).sendKeys(detailsList.get(0).
			 * get(4).toString());
			 */
			TestBase.driver.findElement(By.name("submit")).click();
			TestBase.driver.findElement(By.xpath(UtilityHelper.containsString("Book now", "a"))).click();
		} else {// multiple passengers
			TestBase.PerformActionOnElement("TMFirstTitle", "Type", detailsList.get(0).get(0).toString());
			TestBase.PerformActionOnElement("TMFirstName", "Type", detailsList.get(0).get(1).toString());
			TestBase.PerformActionOnElement("TMLastName", "Type", detailsList.get(0).get(2).toString());
			TestBase.PerformActionOnElement("TMEmailAddress", "Type", detailsList.get(0).get(3).toString());
			TestBase.PerformActionOnElement("TMPhoneNumber", "Type", detailsList.get(0).get(4).toString());
			
			TestBase.PerformActionOnElement("TMSecondTitle", "Type", detailsList.get(0).get(0).toString());
			TestBase.PerformActionOnElement("TMFirstNameSecond", "Type", detailsList.get(0).get(1).toString());
			TestBase.PerformActionOnElement("TMLastNameSecond", "Type", detailsList.get(0).get(2).toString());
			/*
			 * TestBase.driver.findElement(By.name("81c9ee2a97194f50b91bbc5407d81da2_title")
			 * ).sendKeys(detailsList.get(0).get(0).toString());
			 * TestBase.driver.findElement(By.name(
			 * "e1b18cbfc6f8467a8312bf3f7dc40cc5_firstname")).sendKeys(detailsList.get(0).
			 * get(1).toString()); TestBase.driver.findElement(By.name(
			 * "5b8c768b09e24ce394f0549cec0479d4_lastname")).sendKeys(detailsList.get(0).get
			 * (2).toString()); TestBase.driver.findElement(By.name(
			 * "2e32a6f93bb84b0aac3de33f85eeac86_emailaddress")).sendKeys(detailsList.get(0)
			 * .get(3).toString()); TestBase.driver.findElement(By.name(
			 * "7897d5b79abf4028b6ccdafba951a310_phonenumber")).sendKeys(detailsList.get(0).
			 * get(4).toString());
			 * TestBase.driver.findElement(By.name("1aa3ea19bb8d45cc9bca786b659f4511_title")
			 * ).sendKeys(detailsList.get(0).get(0).toString());
			 * TestBase.driver.findElement(By.name(
			 * "e23522ad917741978392ec068d7306b7_firstnamesecond")).sendKeys(detailsList.get
			 * (0).get(1).toString()); TestBase.driver.findElement(By.name(
			 * "a25f42d0810f4281bc9de3981759dbbe_lastnamesecond")).sendKeys(detailsList.get(
			 * 0).get(2).toString());
			 */
	}
	}
	

	public boolean proceesPayment(List<List<String>> paymentDetailsList) throws Exception {
		logger.info("Proceeding with the payment");
		if (paymentDetailsList.get(0).get(2).equalsIgnoreCase(Variables.FULL_AMOUNT)) {
			driver.findElement(By.xpath("//span[contains(text(), 'Pay total amount')]")).click();
		}
		if (paymentDetailsList.get(0).get(1).equalsIgnoreCase(Variables.DEBIT)) {
			//TestBase.waitForElement(debit).click();
			TestBase.PerformActionOnElement("debit", "Click", "");
		} else {
			TestBase.PerformActionOnElement("credit", "Click", "");
			//TestBase.waitForElement(credit).click();
		}
		
		TestBase.driver.findElement(By.name("b4e9eb4168804210a356409dd7acffb5_cardholder name")).sendKeys(paymentDetailsList.get(0).get(2));
		Thread.sleep(3000);
		//TestBase.driver.switchTo().frame("braintree-hosted-field-number");
		TestBase.driver.switchTo().frame(Variables.CARD_NUMBER_FRAME_NAME);
		TestBase.driver.findElement(By.name("credit-card-number")).sendKeys(paymentDetailsList.get(0).get(3));
		TestBase.driver.switchTo().defaultContent();
		TestBase.driver.switchTo().frame(Variables.EXPIRATION_FRAME_NAME);
		TestBase.driver.findElement(By.name("expiration")).sendKeys(paymentDetailsList.get(0).get(4));
		TestBase.driver.switchTo().defaultContent();
		TestBase.driver.switchTo().frame(Variables.CVV_FRAME_NAME);
		TestBase.driver.findElement(By.name("cvv")).sendKeys(paymentDetailsList.get(0).get(5));
		TestBase.driver.switchTo().defaultContent();
		((JavascriptExecutor) TestBase.driver)
				.executeScript("$('ABA0451D0ABB4F468804689EC4433DE5_opt-in-checkbox').removeClass('hide');");
		((JavascriptExecutor) TestBase.driver)
				.executeScript("document.getElementById('ABA0451D0ABB4F468804689EC4433DE5_opt-in-checkbox').click();");
		TestBase.driver.findElement(By.xpath("//button[contains(text(), 'Process Payment')]")).click();
		//return TestBase.driver.findElement(By.cssSelector(".booking-form__success__what-next")).isDisplayed();
		Thread.sleep(15000);
		return TestBase.driver.findElement(By.cssSelector(".booking-form__success__what-next")).isDisplayed();
	}

}
