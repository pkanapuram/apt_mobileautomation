package pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;

import base.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import utils.LogHelper;
import utils.Variables;

public class PaymentsPage extends TestBase{
	
	private AppiumDriver<?> driver;
	private boolean singleRoomType = false;
	public static Logger logger = LogHelper.getLogger(PaymentsPage.class);

	public PaymentsPage(AndroidDriver<?> driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public boolean proceesPayment(List<List<String>> paymentDetailsList) throws InterruptedException {
		logger.info("Proceeding with the payment");
		if (paymentDetailsList.get(0).get(2).equalsIgnoreCase(Variables.FULL_AMOUNT)) {
			driver.findElement(By.xpath("//span[contains(text(), 'Pay Total Amount')]")).click();
		}
		if (paymentDetailsList.get(0).get(1).equalsIgnoreCase(Variables.DEBIT)) {
			TestBase.driver.findElement(By.xpath("//label[contains(text(), 'Debit')]")).click();
		} else {
			TestBase.driver.findElement(By.xpath(
					"//label[contains(text(), 'Credit')]")).click();
		}
		TestBase.driver.findElement(By.name("c0467154a690424f824bd126e709d8f5_cardholder name")).sendKeys(paymentDetailsList.get(0).get(2));
		Thread.sleep(3000);
		//TestBase.driver.switchTo().frame("braintree-hosted-field-number");
		TestBase.driver.switchTo().frame(Variables.CARD_NUMBER_FRAME_NAME);
		TestBase.driver.findElement(By.name("credit-card-number")).sendKeys(paymentDetailsList.get(0).get(3));
		TestBase.driver.switchTo().defaultContent();
		TestBase.driver.switchTo().frame(Variables.EXPIRATION_FRAME_NAME);
		TestBase.driver.findElement(By.name("expiration")).sendKeys(paymentDetailsList.get(0).get(4));
		TestBase.driver.switchTo().defaultContent();
		TestBase.driver.switchTo().frame(Variables.CVV_FRAME_NAME);
		TestBase.driver.findElement(By.name("cvv")).sendKeys(paymentDetailsList.get(0).get(5));
		TestBase.driver.switchTo().defaultContent();
		((JavascriptExecutor) TestBase.driver)
				.executeScript("$('D2A819102BAD4F219CB634FA9ECE760C_opt-in-checkbox').removeClass('hide');");
		((JavascriptExecutor) TestBase.driver)
				.executeScript("document.getElementById('D2A819102BAD4F219CB634FA9ECE760C_opt-in-checkbox').click();");
		TestBase.driver.findElement(By.xpath("//button[contains(text(), 'Process Payment')]")).click();
		//return TestBase.driver.findElement(By.cssSelector(".booking-form__success__what-next")).isDisplayed();
		Thread.sleep(7000);
		return TestBase.driver.findElement(By.xpath("//p[@class='booking-form__success__what-next']")).isDisplayed();
	}
	
	
	
	//NewsletterPage
	public void signUpNewsLetter(List<List<String>> userDetails) {
		TestBase.driver.findElement(By.name("d36b412f86124675badc508a5a542418_title")).sendKeys(userDetails.get(0).get(0));
		TestBase.driver.findElement(By.name("3d93ae1c8955447c92d00d5ae7e997ba_firstname")).sendKeys(userDetails.get(0).get(1));
		TestBase.driver.findElement(By.name("9aa2da40fa274abda635156bc36d24a5_lastname")).sendKeys(userDetails.get(0).get(2));
		TestBase.driver.findElement(By.name("a0c82094abc845adb79661f1df522c61_emailaddress")).sendKeys(userDetails.get(0).get(3));
	}
	
	public void newsletterSignup() {
		TestBase.driver.findElement(By.name("submit")).click();
	}
	
	public String newsletterText() {
		return TestBase.driver.findElement(By.xpath("//h3[contains(text(), 'THANK YOU FOR SIGNING UP TO OUR NEWSLETTER')]")).getText();
	}
	




}
