package pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import base.TestBase;
import io.appium.java_client.android.AndroidDriver;
import utils.LogHelper;
import utils.UtilityHelper;
import utils.Variables;

public class TMSpecialDealsPage  extends TestBase {
	public static Logger logger = LogHelper.getLogger(TMSpecialDealsPage.class);


	public TMSpecialDealsPage(AndroidDriver<?> driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	private WebDriver driver;

	public void searchBySD() throws Exception {
		TestBase.PerformActionOnElement("menu", "Click", "");
		TestBase.PerformActionOnElement("specialDealsTM", "Click", "");
	}

	public boolean selectsTripBySD(String region, String deal) {
		boolean tripfound = false;
		TestBase.driver.findElement(By.xpath(UtilityHelper.containsString(region, "button"))).click();
		TestBase.driver.findElement(By.xpath("(//a[contains(text(), 'View Itineraries')])[2]")).click();
		
		
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		String url= TestBase.driver.getCurrentUrl();
		TestBase.driver.get(url+Variables.ipaddress);
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		
		
		List<WebElement> dealsTrips= (List<WebElement>) TestBase.driver.findElements(By.xpath( "/html/body/div[2]/article[2]/section/div/div/div[1]/section/div[1]/a[2]"));
		for (int k = 1; k <= dealsTrips.size(); k++) {
			if (!(TestBase.driver.findElement(
							By.xpath("/html/body/div[2]/article[2]/section/div/div/div[" + k + "]/section/div[1]/a[2]")))
					.getText().contains(Variables.QUOTE)) {
				TestBase.driver.findElement(
						By.xpath("//*[@id='trip-listing']/div/div/div[" + k + "]/section/div/a[2]")).click();
				tripfound = true;
				break;
			} else {
				logger.info("No Trip's enabled for this Deal");
			}
		}
		return tripfound;
	}
}
