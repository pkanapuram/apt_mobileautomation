package pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import base.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import utils.LogHelper;

public class NewsLetterPage extends TestBase {
	
	private AppiumDriver<?> driver;

	public static Logger logger = LogHelper.getLogger(HomePage.class);
	public NewsLetterPage(AndroidDriver<?> driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}
	
	public void signUpNewsLetter(List<List<String>> userDetails) throws Exception {
		TestBase.PerformActionOnElement("title_NewLetter", "Type", userDetails.get(0).get(0));
		TestBase.PerformActionOnElement("fName_Newsletter", "Type", userDetails.get(0).get(1));
		TestBase.PerformActionOnElement("lName_Newsletter", "Type", userDetails.get(0).get(2));
		TestBase.PerformActionOnElement("emailAddress_Newsletter", "Type", userDetails.get(0).get(3));
		
	}
	
	public void newsletterSignup() throws Exception {
		TestBase.PerformActionOnElement("submit_Newsletter", "Click", "");
		
	}
	
	public String newsletterText() {
		return TestBase.driver.findElement(By.xpath("//h3[contains(text(), 'THANK YOU FOR SIGNING UP TO OUR NEWSLETTER')]")).getText();
		
	}

}
