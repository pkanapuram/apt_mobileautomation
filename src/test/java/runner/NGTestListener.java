package runner;



import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import listeners.ExtentReportListener;


public class NGTestListener extends ExtentReportListener implements ITestListener {

	private static ExtentReports extent;
	

    @Override
    public void onTestStart(ITestResult iTestResult) {
        System.out.println("On test start");
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {

        System.out.println("PASS");
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        System.out.println("FAIL");
		
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        System.out.println("SKIP");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
       
    }

    @Override
    public void onStart(ITestContext iTestContext) {
    	
        System.out.println("On start: Execution started on QA2 environment");
        extent= setUp();
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
    	extent.flush();
        System.out.println("On finish: Generated Report.....");
       
    }
}