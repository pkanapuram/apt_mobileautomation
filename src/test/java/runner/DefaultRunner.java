package runner;

import org.testng.annotations.AfterClass;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import base.TestBase;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
/*
 * @author P.X.Kanapuram
 *
 */

//---------------------------------------ALL Cucumber Options---------------------------------------//
/* @CucumberOptions(features = {"src/test/resources/features/"}, glue
= {"stepDefinations"}, plugin =
{"pretty","html:target/appium_all/cucumber-pretty",
"json:target/appium_all/cucumberTestReports.json",
"json:target/appium_all/cukejson.json",
"testng:target/appium_all/cuketestng.xml",
"rerun:target/appium_all/rerun.txt"})

*/

 //---------------------------------------APT Cucumber Options---------------------------------------//
/* @CucumberOptions(features = {"src/test/resources/features/trip.feature"}, glue
 = {"stepDefinations"}, plugin =
 {"pretty","html:target/appium_apt/cucumber-pretty",
 "json:target/appium_apt/cucumberTestReports.json",
 "json:target/appium_apt/cukejson.json",
 "testng:target/appium_apt/cuketestng.xml",
 "rerun:target/appium_apt/rerun.txt"})

*/
 //---------------------------------------Botanica Cucumber Options---------------------------------------//
/* @CucumberOptions(features = {"src/test/resources/features/botanica.feature"}, glue
 = {"stepDefinations"}, plugin =
 {"pretty","html:target/appium_botanica/cucumber-pretty",
 "json:target/appium_botanica/cucumberTestReports.json",
 "json:target/appium_botanica/cukejson.json",
 "testng:target/appium_botanica/cuketestng.xml",
 "rerun:target/appium_botanica/rerun.txt"})*/
 

 //---------------------------------------Travel Marvel Cucumber Options---------------------------------------//
@CucumberOptions(features = {"src/test/resources/features/TM.feature"}, glue = {"stepDefinations"}, plugin = {"pretty","html:target/appium_tm/cucumber-pretty","json:target/appium_tm/cucumberTestReports.json","json:target/appium_tm/cukejson.json",
"testng:target/appium_tm/cuketestng.xml", "rerun:target/appium_tm/rerun.txt"})


public class DefaultRunner extends AbstractTestNGCucumberTests {
	
	@BeforeClass
	public static void before() {
		System.out.println("Before - "+System.currentTimeMillis());
	}
	
	@AfterClass
	public static void after() {
		TestBase.driver.quit();
		TestBase.stopServer();
		System.out.println("After - "+System.currentTimeMillis());
	}
	
	 @Override
	    @DataProvider
	    //@DataProvider (parallel = true) -- For parallel execution support (which is not going to work for our code)
	    public Object[][] scenarios() {
	        return super.scenarios();
	    }
	 
	

	
	
}