package stepDefinations;

import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;

import base.TestBase;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.BOTBrochurePage;
import pages.HomePage;
import pages.ReqBrochurePage;
import utils.LogHelper;
import utils.Variables;

public class APT_BrochureSD extends TestBase {
	Logger logger = LogHelper.getLogger(this.getClass());
	ReqBrochurePage brochurePage = new ReqBrochurePage(null);
	HomePage homePage= new HomePage(null);

	@Given("^Given user on APTouring page$")
	public void Given_user_on_APTouring_page() {

		logger.info("Launching APT");
		Assert.assertEquals(Variables.APT_TITLE, driver.getTitle());
	}

	@When("^user click on Post Me A broucher for \"(.*)\"$")
	public void user_click_on_Post_Me_A_broucher_for(String broucherCode) throws Exception {
		homePage.searchByCode(broucherCode);
		// brochurePage.clickIcon();
		brochurePage.filterAndSelectBoucher();
		logger.info("Brochure Filters applied");
	}

	@And("^user should enter details$")
	public void user_should_enter_details(io.cucumber.datatable.DataTable details) throws Exception {
		List<List<String>> userDetails = details.asLists();
		brochurePage.enterDetailsForBroucherQuote(userDetails);
		logger.info("User details provided");
		Assert.assertEquals(brochurePage.quoteSent(), true);
		logger.info("APT Brochure Request sent");
	}

	/*
	 * @Then("^quote is successful$") public void quote_should_be_successful()
	 * throws InterruptedException { Assert.assertEquals(brochurePage.quoteSent(),
	 * true); logger.info("APT Brochure Request sent"); }
	 */
}
