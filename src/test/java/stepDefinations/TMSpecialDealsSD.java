package stepDefinations;

import org.apache.log4j.Logger;
import org.testng.Assert;

import base.TestBase;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.BOTSpecialDealsPage;
import pages.TMSpecialDealsPage;
import utils.LogHelper;
import utils.Variables;

public class TMSpecialDealsSD extends TestBase {
	Logger logger = LogHelper.getLogger(this.getClass());
	boolean tripFound = false;
	TMSpecialDealsPage sdPage = new TMSpecialDealsPage(null);

	@Given("^user is on TM Website HomePage$")
	public void user_is_on_TM_Website_HomePage() {
		logger.info("Launching Travel Marvel Website");
		Assert.assertEquals(Variables.TM_TITLE, TestBase.driver.getTitle());
	}

	@When("^user search trip for TM page$")
	public void user_search_trip_for_TM() throws Throwable {
		logger.info("searching trips with Special Deals");
		sdPage.searchBySD();
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		String url= TestBase.driver.getCurrentUrl();
		TestBase.driver.get(url+Variables.ipaddress);
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
	}

	@Then("^search trip \"(.*)\" by Special Deals \"(.*)\"$")
	public void search_the_trip_by_Special_Deals(String region, String deal) throws Throwable {
		logger.info("Clicking on SD trips");
		tripFound = sdPage.selectsTripBySD(region, deal);
	}

}
