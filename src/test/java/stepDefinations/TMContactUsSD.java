package stepDefinations;

import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;

import base.TestBase;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.BOTContactUsPage;
import pages.HomePage;
import pages.TMContactUsPage;
import utils.LogHelper;
import utils.Variables;

public class TMContactUsSD extends TestBase {
	
	Logger log = LogHelper.getLogger(this.getClass());
	HomePage homePage= new HomePage(null);
	TMContactUsPage contactUSPage= new TMContactUsPage(null);
	
	
	@Given("user is on the TM page")
	public void user_on_TM_Home_page() {
		System.out.println("**************************CONTACT US**************************");
		log.info("**************************TM- CONTACT US**************************");
		Assert.assertEquals(Variables.TM_TITLE, TestBase.driver.getTitle());
	}

	@When("user click on contact us for TM Page")
	public void user_clicks_on_contactus() throws InterruptedException {
		contactUSPage.contactUs();
		log.info("User clicks on Contact Us");
		
	}

	@Then("select for TM Page \"(.*)\"$")
	public void select_Topic(String topicId) throws Exception {
		Thread.sleep(3000);
		//--------------------------------------Comment the below piece of code if AUD IP Address appending is not required---------------------------------------
		String url=TestBase.driver.getCurrentUrl();
		 String url1 = url+ Variables.ipaddress;
		 driver.get(url1);
		//---------------------------------------Comment the above piece of code if AUD IP Address appending is not required---------------------------------------
		 contactUSPage.topic(topicId);
		 log.info("Topic ID is Selected");
		 
	}

	@Then("enters message for TM Page \"(.*)\"$")
	public void enter_message(String msg) throws Exception {
		contactUSPage.sendMessage(msg);
		log.info("Enter Message");
	}

	@Then("enters details for TM Page \"(.*)\"$")
	public void enter_details_for_TM_Page(io.cucumber.datatable.DataTable details) throws Exception {
		List<List<String>> userDetails = details.asLists();
		contactUSPage.sendEnquiryDetails(userDetails);
		log.info("Enquiry Details Provided");
	}

	@Then("email will be sent for TM Page \"(.*)\"$")
	public void email_will_be_sent_for_TM_Page() throws InterruptedException {
		Assert.assertTrue(contactUSPage.success().equalsIgnoreCase("THANK YOU"));
		log.info("ContactUs email is sent");
		Thread.sleep(3000);
	}

}
