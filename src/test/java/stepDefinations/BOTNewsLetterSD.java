package stepDefinations;

import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;

import base.TestBase;
import pages.BOTNewsletterPage;
import utils.Variables;
import utils.LogHelper;
import utils.UtilityHelper;


import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
/*
 * @author P.X.Kanapuram
 *
 */
public class BOTNewsLetterSD extends TestBase {
	Logger logger = LogHelper.getLogger(this.getClass());
	BOTNewsletterPage newsLetterPage = new BOTNewsletterPage(null);

	@Given("^user on Botanica page$")
	public void user_on_APT_page() {
		logger.info("Launching Botanica Website");
		Assert.assertEquals(Variables.BOTANICA_TITLE, TestBase.driver.getTitle());
	}

	@When("^user provides details$")
	public void user_enter_details(io.cucumber.datatable.DataTable details) throws Exception {
		List<List<String>> userDetails = details.asLists();
		logger.info("User details for Newsletter are provided");
		newsLetterPage.signUpNewsLetter(userDetails);
	}

	@And("^clicks on SignUp$")
	public void click_on_SignUp() throws Exception {
		newsLetterPage.newsletterSignup();
		logger.info("Clicked on Newsletter Sign Up");
	}

	@Then("^user will be signed up$")
	public void user_should_be_signed_up() {
		Assert.assertEquals(Variables.BOTANICA_THANK_YOU_FOR_SIGNING_UP_TO_OUR_NEWSLETTER,
				newsLetterPage.newsletterText());
		logger.info("Newsletter for Botanica Website is signedUp successfully");
	}
}