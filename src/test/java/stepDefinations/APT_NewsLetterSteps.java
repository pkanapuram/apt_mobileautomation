package stepDefinations;

import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;

import base.TestBase;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
//import io.cucumber.java.en.And;
//import io.cucumber.java.en.Given;
//import io.cucumber.java.en.Then;
//import io.cucumber.java.en.When;
import listeners.ExtentReportListener;
import pages.NewsLetterPage;

import utils.LogHelper;
import utils.Variables;

public class APT_NewsLetterSteps extends ExtentReportListener { // ExtentReportListener
	Logger log = LogHelper.getLogger(this.getClass());
	private NewsLetterPage newsLetterPage;
	ExtentReportListener extentReports;


	public APT_NewsLetterSteps() {
		this.newsLetterPage = new NewsLetterPage(null);
	}

	@Given("user on the APT page")
	public void user_on_the_APT_page() throws Throwable {
		ExtentTest logInfo = null;
		log.info("**************************NEWSLETTER**************************");
		/*
		 * try { test = extent.createTest(Feature.class, "News Letter Signup"); test =
		 * test.createNode(Scenario.class, "News Letter Signup"); logInfo =
		 * test.createNode(new GherkinKeyword("Given"), "user on the APT page");
		 */
			Assert.assertEquals(Variables.APT_TITLE, TestBase.driver.getTitle());
		/*
		 * logInfo.pass("User is on the APT Home Page"); logInfo.pass("Newsletter"); }
		 * catch (AssertionError | Exception e) { testStepHandle("FAIL",
		 * TestBase.driver, logInfo, e); }
		 */

	}

	@When("user enter the details")
	public void user_enter_the_details(io.cucumber.datatable.DataTable data) throws Exception {
		/*
		 * ExtentTest logInfo = null; try { logInfo = test.createNode(new
		 * GherkinKeyword("When"), "user enter the details");
		 */
			List<List<String>> userDetails = data.asLists();
			newsLetterPage.signUpNewsLetter(userDetails);
			log.info("User Details for Newsletter are provided");
		/*
		 * logInfo.pass("Newsletter details are entered"); } catch (AssertionError |
		 * Exception e) { testStepHandle("FAIL", TestBase.driver, logInfo, e); }
		 */

	}

	@When("click on Signup")
	public void click_on_Signup() throws Exception {
		/*
		 * ExtentTest logInfo = null; try { logInfo = test.createNode(new
		 * GherkinKeyword("When"), "click on Signup");
		 */
			newsLetterPage.newsletterSignup();
			log.info("Clicked on Newsletter Sign Up");
		/*
		 * logInfo.pass("Click on signup"); } catch (AssertionError | Exception e) {
		 * testStepHandle("FAIL", TestBase.driver, logInfo, e); }
		 */

	}

	@Then("user should be signed up")
	public void user_should_be_signed_up() throws Throwable {
		/*
		 * ExtentTest logInfo = null; try { logInfo = test.createNode(new
		 * GherkinKeyword("Then"), "user should be signed up");
		 */
			Assert.assertEquals(Variables.THANK_YOU_FOR_SIGNING_UP_TO_OUR_NEWSLETTER, newsLetterPage.newsletterText());
			log.info("Newsletter Signed");
			Thread.sleep(3000);
			/*logInfo.pass("Newsletter signedUp");
		} catch (AssertionError | Exception e) {
			testStepHandle("FAIL", TestBase.driver, logInfo, e);
		}*/

	}

}
