package stepDefinations;

import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;

import base.TestBase;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.BOTNewsletterPage;
import pages.TMNewsletterPage;
import utils.LogHelper;
import utils.Variables;

public class TMNewsletterSD extends TestBase {
	Logger logger = LogHelper.getLogger(this.getClass());
	TMNewsletterPage newsLetterPage = new TMNewsletterPage(null);

	@Given("^user on TM page$")
	public void user_on_APT_page() {
		logger.info("Launching TM Website");
		Assert.assertEquals(Variables.TM_TITLE, TestBase.driver.getTitle());
	}

	@When("^user provides details on TM$")
	public void user_enter_details(io.cucumber.datatable.DataTable details) throws Exception {
		List<List<String>> userDetails = details.asLists();
		logger.info("User details for Newsletter are provided");
		newsLetterPage.signUpNewsLetter(userDetails);
	}

	@And("^clicks on SignUp for TM$")
	public void click_on_SignUp_for_TM() throws Exception {
		newsLetterPage.newsletterSignup();
		logger.info("Clicked on Newsletter Sign Up");
	}

	@Then("^user will be signed up for TM$")
	public void user_should_be_signed_up_for_TM() {
		Assert.assertEquals(Variables.BOTANICA_THANK_YOU_FOR_SIGNING_UP_TO_OUR_NEWSLETTER,
				newsLetterPage.newsletterText());
		logger.info("Newsletter for TM Website is signedUp successfully");
	}
}