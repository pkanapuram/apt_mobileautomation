package stepDefinations;

import org.apache.log4j.Logger;
import org.testng.Assert;

/*import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.Scenario;*/

import base.TestBase;
//import base.TestBase2;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import pages.HomePage;
import utils.LogHelper;
//import pages.HomePage_Rag;
import utils.Variables;

public class APT_SpecialDealsSteps extends TestBase{
	Logger log = LogHelper.getLogger(this.getClass());
	
	private HomePage homePage= new HomePage(null);
	boolean tripFound = false;

@Given("user is on APT page")
public void user_is_on_APT_page() {
	log.info("**************************SPECIALDEALS**************************");
		Assert.assertEquals(Variables.APT_TITLE, TestBase.driver.getTitle());
		
}
   


@When("user searchs trip")
public void user_searchs_trip() {
			homePage.searchBySD();
			log.info("Searching Deals");
		}
    


@Then("search the {string} trip by Special Deals {string}")
public void search_the_trip_by_Special_Deals(String region, String deal) throws InterruptedException {

	//--------------------------------------Comment the  piece of code if AUD IP Address appending is not required---------------------------------------
	String url=TestBase.driver.getCurrentUrl();
	 String url1 = url+ Variables.ipaddress;
	 TestBase.driver.get(url1);
	//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		tripFound = homePage.selectsTripBySD(region, deal);
		log.info("Trips with Special Deals is Selected");
		Thread.sleep(3000);
		
  
}

}
