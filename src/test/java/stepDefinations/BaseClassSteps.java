package stepDefinations;

import java.io.File;
import java.net.MalformedURLException;

import com.relevantcodes.extentreports.ExtentReports;

import ExtentReportListener.ExtentReporterNG;
import base.TestBase;
//import base.TestBase2;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import listeners.ExtentReportListener;
/*
 * @author P.X.Kanapuram
 *
 */
public class BaseClassSteps extends TestBase{  //ExtentReportListener
	ExtentReportListener erl= new ExtentReportListener();  //TestBase- changed 04July2020
	ExtentReporterNG er= new ExtentReporterNG();
	String outputDirectory;
	public static String featureName;
	
	  @Before
		public void before(Scenario scenario) throws MalformedURLException, InterruptedException {
		  ExtentReportListener.setUp();  // to be commented when extends  TestBase
		
	
			TestBase.setUpDriver();
			System.out.println("Test Case execution is started");
		}
	  
	  @After
	  public void quit() {
		TestBase.driver.quit();
		TestBase.stopServer();
		System.out.println("Test Case execution is completed");}}