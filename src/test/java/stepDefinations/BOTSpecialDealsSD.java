package stepDefinations;

import org.apache.log4j.Logger;
import org.testng.Assert;

import base.TestBase;
import pages.BOTSpecialDealsPage;
import utils.Variables;
import utils.LogHelper;
import utils.UtilityHelper;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
/*
 * @author P.X.Kanapuram
 *
 */
public class BOTSpecialDealsSD extends TestBase {
	Logger logger = LogHelper.getLogger(this.getClass());
	boolean tripFound = false;
	BOTSpecialDealsPage sdPage = new BOTSpecialDealsPage(null);

	@Given("^user is on Botanica page$")
	public void user_is_on_APT_page() {
		logger.info("Launching Botanica Website");
		Assert.assertEquals(Variables.BOTANICA_TITLE, TestBase.driver.getTitle());
	}

	@When("^user search trip$")
	public void user_search_trip() throws Throwable {
		logger.info("searching trips with Special Deals");
		sdPage.searchBySD();
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		String url= TestBase.driver.getCurrentUrl();
		TestBase.driver.get(url+Variables.ipaddress);
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
	}

	@Then("^search \"(.*)\" trip by Special Deals \"(.*)\"$")
	public void search_the_trip_by_Special_Deals(String region, String deal) throws Throwable {
		logger.info("Clicking on SD trips");
		tripFound = sdPage.selectsTripBySD(region, deal);
	}

}
