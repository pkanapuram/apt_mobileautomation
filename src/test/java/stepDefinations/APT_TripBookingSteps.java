package stepDefinations;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.testng.Assert;

import base.TestBase;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utils.LogHelper;

import pages.HomePage;
import pages.NewsLetterPage;
import pages.PaymentsPage;
import pages.TripBookingPage;
import utils.Variables;

@SuppressWarnings("deprecation")
public class APT_TripBookingSteps extends TestBase {	
	Logger log = LogHelper.getLogger(this.getClass());
	private HomePage homePage= new HomePage(null);
	private TripBookingPage tripBookingPage = new TripBookingPage(null);
	private PaymentsPage paymentsPage= new PaymentsPage(null);
	String url = driver.getCurrentUrl();

	/*
	 * public TripBookingSteps() { this.tripBookingPage = new TripBookingPage(null);
	 * this.paymentsPage = new PaymentsPage(null); }
	 */
	

	@Given("^user on APT page$")
	public void user_on_APT_page() {
		log.info("**************************TRIP BOOKING**************************");
		Assert.assertEquals(Variables.APT_TITLE, TestBase.driver.getTitle());
	}

	@When("^user enter trip code \"(.*)\"$")
	public void user_enter_trip_code(String tripCode) throws Exception {
		homePage.searchByCode(tripCode.trim());
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		String url=TestBase.driver.getCurrentUrl();
		 String url1 = url+ Variables.ipaddress2;
		 TestBase.driver.get(url1);
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		 log.info("TripCode is Provided");
		
		
	}

	@Then("^user should see the trip \"(.*)\"$")
	public void user_should_see_the_trip(String trip) {
		TestBase.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		TestBase.driver.getTitle().toString().contains(trip.trim());					
		log.info("Searched Trip found!");
		TestBase.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
		 String url=TestBase.driver.getCurrentUrl(); 
		  String url1 = url+Variables.ipaddress; 
		  System.out.println(url1); 
		  TestBase.driver.get(url1);
		 
		 
		
		}

	@Then("^user clicks on the trip$")
	public void user_clicks_on_the_trip() throws Exception {
		
		 TestBase.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
		 
		 tripBookingPage.clickOnTrip();
		 log.info("Trip is Selected");
		}

	@And("^user enters date$")
	public void user_enters_date(io.cucumber.datatable.DataTable dates) throws InterruptedException {
		List<List<String>> dateList = dates.asLists();
		tripBookingPage.selectPrice_Availability(dateList);
		log.info("Price Availability Selected");
	}

	@Then("^user selects roomtype \"(.*)\" and cabin \"(.*)\"$")
	public void user_selects_roomtype_and_cabin(String roomType, String cabinType) throws Throwable {
		tripBookingPage.selectRoomType(roomType.trim(), cabinType.trim());
		log.info("Cabin Type Selected");
	}

	@Then("^user enters personal details$")
	public void user_enters_personal_details(io.cucumber.datatable.DataTable data) {
		List<List<String>> detailsList = data.asLists();
		tripBookingPage.enterBookingDetails(detailsList);
		log.info("Booking Details Provided");
	}

	@Then("^user enter paymentdetails and Payment should be successful$")
	public void user_enter_paymentdetails_and_Payment_should_be_successful(io.cucumber.datatable.DataTable data) throws InterruptedException {
		List<List<String>> paymentDetailsList = data.asLists();
		boolean bookingSuccess = paymentsPage.proceesPayment(paymentDetailsList);
		Assert.assertEquals(bookingSuccess, true);
		log.info("Trip Booking is successful");
		Thread.sleep(3000);
	}

}
