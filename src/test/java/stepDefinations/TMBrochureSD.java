package stepDefinations;

import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;

import base.TestBase;
import pages.HomePage;
import pages.TMBrochurePage;
import utils.Variables;
import utils.LogHelper;
import utils.UtilityHelper;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TMBrochureSD extends TestBase {
	Logger logger = LogHelper.getLogger(this.getClass());
	TMBrochurePage brochurePage = new TMBrochurePage(null);

	@Given("^user on the TM page$")
	public void user_on_SiteCore_page() {
		logger.info("Launching Travel Marvel Website");
		System.out.println(TestBase.driver.getTitle());
		Assert.assertEquals(Variables.TM_TITLE, TestBase.driver.getTitle());
	}

	@When("^user click on TM Post Me A broucher for \"(.*)\"$")
	public void user_click_on_Post_Me_A_broucher_for(String broucherCode) throws Exception {
		brochurePage.searchByCode(broucherCode);
		brochurePage.filterAndSelectBrochure();
		logger.info("Applied Filter selections");
	}

	@And("^user should enter details on TM$")
	public void user_should_enter_details(io.cucumber.datatable.DataTable details) throws Exception {
		List<List<String>> userDetails = details.asLists();
		brochurePage.enterDetails(userDetails);
		logger.info("Brochure Details are Provided");
		Thread.sleep(3000);
	}

	@Then("^quote should be successful on TM$")
	public void quote_should_be_successful() throws Exception {
		Assert.assertEquals(brochurePage.quoteSent(), true);
		logger.info("TM Brochure Request sent");
		Thread.sleep(5000);
	}

}
