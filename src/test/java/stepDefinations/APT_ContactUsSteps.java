package stepDefinations;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import base.TestBase;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.ContactUsPage;
import pages.HomePage;
import utils.LogHelper;
import utils.Variables;

public class APT_ContactUsSteps extends TestBase {
	
	Logger log = LogHelper.getLogger(this.getClass());
	HomePage homePage= new HomePage(null);
	ContactUsPage contactUSPage= new ContactUsPage(null);
	
	
	@Given("user on APT Home page")
	public void user_on_APT_Home_page() {
		System.out.println("**************************CONTACT US**************************");
		log.info("**************************APT- CONTACT US**************************");
		Assert.assertEquals(Variables.APT_TITLE, TestBase.driver.getTitle());
	}

	@When("user clicks on contactus")
	public void user_clicks_on_contactus() {
		contactUSPage.contactUs();
		log.info("User clicks on Contact Us");
		
	}

	@Then("select Topic \"(.*)\"$")
	public void select_Topic(String topicId) throws Exception {
		Thread.sleep(3000);
		//--------------------------------------Comment the below piece of code if AUD IP Address appending is not required---------------------------------------
		String url=TestBase.driver.getCurrentUrl();
		 String url1 = url+ Variables.ipaddress;
		 driver.get(url1);
		//---------------------------------------Comment the above piece of code if AUD IP Address appending is not required---------------------------------------
		 contactUSPage.topic(topicId);
		 log.info("Topic ID is Selected");
		 
	}

	@Then("enter message \"(.*)\"$")
	public void enter_message(String msg) throws Exception {
		contactUSPage.sendMessage(msg);
		log.info("Enter Message");
	}

	@Then("enter details")
	public void enter_details(io.cucumber.datatable.DataTable details) throws Exception {
		List<List<String>> userDetails = details.asLists();
		contactUSPage.sendEnquiryDetails(userDetails);
		log.info("Enquiry Details Provided");
	}

	@Then("email should be sent")
	public void email_should_be_sent() throws InterruptedException {
		Assert.assertTrue(contactUSPage.success().equalsIgnoreCase("THANK YOU"));
		log.info("ContactUs email is sent");
		Thread.sleep(3000);
	}

}
