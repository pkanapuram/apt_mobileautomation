package stepDefinations;

import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;

import base.TestBase;
import pages.BOTSearchFilterPage;
import utils.Variables;
import utils.LogHelper;
import utils.UtilityHelper;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
/*
 * @author P.X.Kanapuram
 *
 */
public class BOTSearchFilterSD extends TestBase {
	Logger logger = LogHelper.getLogger(this.getClass());
	BOTSearchFilterPage filterPage = new BOTSearchFilterPage(null);
	boolean tripFound = false;

	@Given("^user on Botanica$")
	public void user_on_Botanica_Website() {
		logger.info("Launching Botanica Website");
		Assert.assertEquals(Variables.BOTANICA_TITLE, TestBase.driver.getTitle());
	}

	@When("^user search trip by filter$")
	public void user_search_trip_by_filter(DataTable data) throws Throwable {
		logger.info("filtering trips");
		List<List<String>> searchList = data.asLists();
		filterPage.searchTrip(searchList);
	}

	@Then("^validate results$")
	public void selects_the_trip(DataTable data) throws Throwable {
		List<List<String>> list = data.asLists();
		logger.info("Trips are validated");
		filterPage.selectTrip(list);
	}
}
