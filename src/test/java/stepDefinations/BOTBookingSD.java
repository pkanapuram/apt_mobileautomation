package stepDefinations;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.testng.Assert;

import base.TestBase;
import pages.BOTBookingPage;
import utils.Variables;
import utils.LogHelper;
import utils.UtilityHelper;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
/*
 * @author P.X.Kanapuram
 *
 */
public class BOTBookingSD extends TestBase {
	Logger logger = LogHelper.getLogger(this.getClass());
	BOTBookingPage botanicaBooking = new BOTBookingPage(null);

	@Given("^user on Botanica Website$")
	public void user_on_Botanica_page() {
		logger.info("Launching Botanica");
		
		Assert.assertEquals(Variables.BOTANICA_TITLE, TestBase.driver.getTitle());
	}

	@When("^user search by trip code \"(.*)\"$")
	public void user_enter_trip_code(String tripCode) {
		botanicaBooking.searchByCode(tripCode.trim());
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		String url=TestBase.driver.getCurrentUrl();
		 String url1 = url+ Variables.ipaddress;
		 TestBase.driver.get(url1);
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		 logger.info("TripCode is Provided");
	}

	@Then("^choose trip$")
	public void user_clicks_on_the_trip() {
		botanicaBooking.clickOnTrip();
		logger.info("Click on Trip");
	}

	@And("^user choose availability$")
	public void user_enters_date(DataTable dates) {
		List<List<String>> dateList = dates.asLists();
		botanicaBooking.selectPrice_Availability(dateList);
		logger.info("Select Price Availability for Botanica");
	}

	@Then("^user enter room \"(.*)\"$")
	public void user_enter_detail(String room) throws Exception {
		botanicaBooking.selectRoomType(room);
		logger.info("RoomType for Botanica is Selected");
	}

	@Then("^user enter detail$")
	public void user_enter_detail(DataTable data) {
		List<List<String>> detailsList = data.asLists();
		botanicaBooking.enterDetails(detailsList);
		logger.info("Trip Booking- user details for Botanica are provided");
	}

	@Then("^user enters payment details and Payment should be successful$")
	public void user_enters_payment_details_and_Payment_should_be_successful(DataTable data) throws Exception {
		List<List<String>> paymentDetailsList = data.asLists();
		boolean bookingSuccess = botanicaBooking.proceesPayment(paymentDetailsList);
		Assert.assertEquals(bookingSuccess, true);
		logger.info("Trip Booking for Botanica is successful");
	}
}