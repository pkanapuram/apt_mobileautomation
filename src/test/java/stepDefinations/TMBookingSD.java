package stepDefinations;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.testng.Assert;

import base.TestBase;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import pages.BOTBookingPage;
import pages.TMBookingPage;
import utils.LogHelper;
import utils.Variables;

public class TMBookingSD extends TestBase {
	Logger logger = LogHelper.getLogger(this.getClass());
	TMBookingPage TMBooking = new TMBookingPage(null);

	@Given("^User on TM HomePage$")
	public void user_on_TM_HomePage() {
		logger.info("Launching Travel Marvel Website");
		Assert.assertEquals(Variables.TM_TITLE, TestBase.driver.getTitle());
	}

	@When("^user search by trip code for TM Page \"(.*)\"$")
	public void user_enter_trip_code(String tripCode) {
		TMBooking.searchByCode(tripCode.trim());
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		String url=TestBase.driver.getCurrentUrl();
		 String url1 = url+ Variables.ipaddress;
		 TestBase.driver.get(url1);
		//--------------------------------------Comment the piece of code if AUD IP Address appending is not required---------------------------------------
		 logger.info("TripCode is Provided");
	}

	@When("^choose trip for TM Page$")
	public void user_clicks_on_the_trip() {
		logger.info("Clicking on Trip price");
		TMBooking.clickOnTrip();
	}

	@And("^user choose availability for TM Page$")
	public void user_enters_date(DataTable dates) {
		List<List<String>> dateList = dates.asLists();
		logger.info("Selecting the price availability");
		TMBooking.selectPrice_Availability(dateList);
	}

	@And("^user enter room for TM Page \"(.*)\"$")
	public void user_enter_detail(String room) throws Exception {
		logger.info("Selecting the room type");
		TMBooking.selectRoomType(room);
	}

	@And("^user enter details for TM Page$")
	public void user_enter_detail(DataTable data) throws Exception {
		List<List<String>> detailsList = data.asLists();
		logger.info("Providing the user booking details");
		TMBooking.enterDetails(detailsList);
	}

	@Then("^user enters payment details for TM Page and Payment should be successful$")
	public void user_enters_payment_details_and_Payment_should_be_successful(DataTable data) throws Exception {
		List<List<String>> paymentDetailsList = data.asLists();
		boolean bookingSuccess = TMBooking.proceesPayment(paymentDetailsList);
		Assert.assertEquals(bookingSuccess, true);
		logger.info("Booking successful");
	}
}
