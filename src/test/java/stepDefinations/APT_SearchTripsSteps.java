package stepDefinations;

import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;

import base.TestBase;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import pages.HomePage;
import pages.PaymentsPage;
import pages.TripBookingPage;
import utils.LogHelper;
import utils.Variables;



public class APT_SearchTripsSteps extends TestBase {
	Logger log = LogHelper.getLogger(this.getClass());
	boolean tripFound = false;
	HomePage homePage = new HomePage(null);
	TripBookingPage tripBookingPage = new TripBookingPage(null);
	//PaymentsPage paymentPage = new PaymentsPage(null);

	@Given("^user on apt home page$")
	public void user_is_on_APT_page() {
		log.info("**************************SEARCH TRIPS**************************");
		Assert.assertEquals(Variables.APT_TITLE, TestBase.driver.getTitle());
	}

	@When("^user search trip by filters$")
	public void user_search_trip_by_filter(DataTable data) throws Throwable {
		log.info("filtering trips");
		List<List<String>> searchList = data.asLists();
		homePage.searchTrips(searchList);
		
	}

	@Then("^validates results$")
	public void selects_the_trip(DataTable data) throws Throwable {
		List<List<String>> list = data.asLists();
		homePage.verifyTrips(list);
		log.info("Trips Searched are validated");
		Thread.sleep(3000);
	}

}
