package stepDefinations;

import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;

import pages.BOTBrochurePage;

import base.TestBase;
import pages.BOTBrochurePage;
import utils.Variables;
import utils.LogHelper;
import utils.UtilityHelper;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
/*
 * @author P.X.Kanapuram
 *
 */
public class BOTBrochureSD extends TestBase {
	Logger logger = LogHelper.getLogger(this.getClass());
	BOTBrochurePage brochurePage = new BOTBrochurePage(null);

	@Given("^user on the Botanica page$")
	public void user_on_Botanica_page() {

		logger.info("Launching Botanica");
		Assert.assertEquals(Variables.BOTANICA_TITLE, driver.getTitle());
	}

	@When("^user click on broucher for \"(.*)\"$")
	public void user_click_on_Post_Me_A_broucher_for(String broucherCode) throws Exception {
		brochurePage.searchByCode(broucherCode);
		// brochurePage.clickIcon();
		brochurePage.filterAndSelectBoucher();
		logger.info("Brochure Filters applied");
	}

	@And("^user enter details$")
	public void user_should_enter_details(io.cucumber.datatable.DataTable details) throws Exception {
		List<List<String>> userDetails = details.asLists();
		brochurePage.enterDetailsForBroucherQuote(userDetails);
		logger.info("User details provided");
	}

	@Then("^quote is successful$")
	public void quote_should_be_successful() throws InterruptedException {
		Assert.assertEquals(brochurePage.quoteSent(), true);
		logger.info("Botanica Brochure Request sent");
	}
}
