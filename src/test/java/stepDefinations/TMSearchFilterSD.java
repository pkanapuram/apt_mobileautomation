package stepDefinations;

import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Assert;

import base.TestBase;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import pages.TMSearchFilterPage;
import utils.LogHelper;
import utils.Variables;

public class TMSearchFilterSD extends TestBase {
	Logger logger = LogHelper.getLogger(this.getClass());
	TMSearchFilterPage filterPage = new TMSearchFilterPage(null);
	boolean tripFound = false;

	@Given("^user is on TM page$")
	public void user_on_TM_page() {
		logger.info("Launching Travel Marvel Website");
		Assert.assertEquals(Variables.TM_TITLE, TestBase.driver.getTitle());
	}

	@When("^user search trip by filter for TM$")
	public void user_search_trip_by_filter_for_TM(DataTable data) throws Throwable {
		logger.info("filtering trips");
		List<List<String>> searchList = data.asLists();
		filterPage.searchTrip(searchList);
	}

	@Then("^validate results for TM$")
	public void selects_the_trip_for_TM(DataTable data) throws Throwable {
		List<List<String>> list = data.asLists();
		filterPage.verifyTrip(list);
		logger.info("Filters are applied and trips are displayed accordingly");
	}
}
