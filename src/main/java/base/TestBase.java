package base;

import java.io.File;
/*
 * @author P.X.Kanapuram
 *
 */

import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
//import org.junit.After;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.AfterMethod;

import com.google.common.collect.ImmutableMap;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import io.cucumber.core.api.Scenario;

public class TestBase<V> {

	public static WebDriver driver = null;
	public SessionId session = null;
	public static Properties prop = new Properties();
	static AppiumDriverLocalService appiumService;
	static String appiumServiceUrl;

	public TestBase() {
		try {
			prop.load(new FileInputStream("./src/test/resources/properties/config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public WebDriver getDriver() {
		return this.getDriver();
	}

	public void setDriver(WebDriver driver) {
		TestBase.driver = driver;
	}

	public static void setUpDriver() throws MalformedURLException, InterruptedException {

		String Appium_Node_Path = (String) prop.get("nodeJS_Path");
		String Appium_JS_Path = (String) prop.get("appium_Path");

		/*---------------------------------------To Start Appium Server on SINGLE PORT---------------------------------------*/
		/*
		 * appiumService = AppiumDriverLocalService.buildDefaultService();
		 * appiumService.start(); appiumServiceUrl = appiumService.getUrl().toString();
		 * System.out.println("Appium Service Address : - " + appiumServiceUrl);
		 */
		/*---------------------------------------To Start Appium Server on SINGLE PORT---------------------------------------*/

		/*---------------------------------------To Start Appium Server ANY FREE PORT---------------------------------------*/

		appiumService = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
				.usingDriverExecutable(new File(Appium_Node_Path)).withAppiumJS(new File(Appium_JS_Path))
				.withIPAddress("127.0.0.1").usingAnyFreePort().withArgument(GeneralServerFlag.SESSION_OVERRIDE));
		appiumService.start();

		/*---------------------------------------To Start Appium Server ANY FREE PORT---------------------------------------*/
		// Set Capabilites for Mobile Components
		DesiredCapabilities dc = new DesiredCapabilities();
		String browser = (String) prop.get("browser");
		String device = (String) prop.get("device");
		// CHANGE THE STRING URL ACCORDING TO WEBSITE
		String appurl = (String) prop.get("travelMarvelURL");		//change the "travelMarvelURL" from config.propeties for required websites as needed
		dc.setCapability(MobileCapabilityType.PLATFORM_NAME, "ANDROID");
		dc.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9");
		dc.setCapability(MobileCapabilityType.DEVICE_NAME, device);
		dc.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
		dc.setCapability(MobileCapabilityType.BROWSER_NAME, browser);
		// dc.setCapability("maxTypingFrequency", 25);
		// dc.setCapability("autoAcceptAlerts", false);
		dc.setCapability("appium:chromeOptions", ImmutableMap.of("w3c", false));
		dc.setCapability("chromedriverExecutable",
				"C:/Users/P.X.kanapuram/git/appium/APT_MobileAutomation/chromedriver.exe");
		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		driver = new AndroidDriver<>(url, dc);
		driver.get(appurl + "?ip=116.240.204.120");
		Thread.sleep(3500);

		/*---------------------------------------Enable for Botanica & TM---------------------------------------*/
		
		  driver.findElement(By.
		  xpath("//*[@class='secondary-button small-link' and contains(text(),'Advanced')]"
		  )) .click(); Thread.sleep(3000);
		  driver.findElement(By.xpath("//a[@href='#' and @id='proceed-link']")).click()
		  ; Thread.sleep(3000);}
		 
		/*---------------------------------------Enable for Botanica & TM---------------------------------------*/

		/*---------------------------------------Enable for APT---------------------------------------*/
		
		 /* if (driver.findElement(By.
		  xpath("//ul[@class='geo-location-debug-header user-message']")).isDisplayed()
		  ) { System.out.println("Displayed is true"); } else
		  System.out.println("Not Displayed"); }*/
		 
		/*---------------------------------------Enable for APT---------------------------------------*/
	

	public void closeDriver(Scenario scenario) {
		if (scenario.isFailed()) {
			saveScreenshotsForScenario(scenario);
		}
		if (driver != null) {
			driver.close();
		}
	}

	private void saveScreenshotsForScenario(final Scenario scenario) {
		final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
		scenario.embed(screenshot, "image/png");
	}

	public void waitForPageLoad(int timeout) {
		ExpectedConditions.jsReturnsValue("return document.readyState==\"complete\";");
	}

	/*---------------------------------------To STOP Appium Server---------------------------------------*/
	//@AfterMethod
	public static void stopServer() {
		driver.quit();
		System.out.println("Appium server stopped");
		appiumService.stop();
	}

	/*---------------------------------------User defined properties---------------------------------------*/
	public static By getElementWithLocator(String WebElement) throws Exception {
		String locatorTypeAndValue = prop.getProperty(WebElement);
		String[] locatorTypeAndValueArray = locatorTypeAndValue.split(",");
		String locatorType = locatorTypeAndValueArray[0].trim();
		String locatorValue = locatorTypeAndValueArray[1].trim();
		switch (locatorType.toUpperCase()) {
		case "ID":
			return By.id(locatorValue);
		case "NAME":
			return By.name(locatorValue);
		case "TAGNAME":
			return By.tagName(locatorValue);
		case "LINKTEXT":
			return By.linkText(locatorValue);
		case "PARTIALLINKTEXT":
			return By.partialLinkText(locatorValue);
		case "XPATH":
			return By.xpath(locatorValue);
		case "CSS":
			return By.cssSelector(locatorValue);
		case "CLASSNAME":
			return By.className(locatorValue);
		default:
			return null;
		}
	}

	public static WebElement FindAnElement(String WebElement) throws Exception {
		return driver.findElement(getElementWithLocator(WebElement));
	}

	public static void PerformActionOnElement(String WebElement, String Action, String Text) throws Exception {
		switch (Action) {
		case "Click":
			FindAnElement(WebElement).click();
			break;
		case "Type":
			FindAnElement(WebElement).sendKeys(Text);
			break;
		case "Clear":
			FindAnElement(WebElement).clear();
			break;
		case "WaitForElementDisplay":
			waitForCondition("Presence", WebElement, 60);
			break;
		case "WaitForElementClickable":
			waitForCondition("Clickable", WebElement, 60);
			break;
		case "ElementNotDisplayed":
			waitForCondition("NotPresent", WebElement, 60);
			break;
		default:
			throw new IllegalArgumentException("Action \"" + Action + "\" isn't supported.");
		}
	}

	public static void waitForCondition(String TypeOfWait, String WebElement, int Time) {
		try {
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Time, TimeUnit.SECONDS)
					.pollingEvery(5, TimeUnit.SECONDS).ignoring(Exception.class);
			switch (TypeOfWait) {
			case "PageLoad":
				wait.until(ExpectedConditions.jsReturnsValue("return document.readyState==\"complete\";"));
				break;
			case "Clickable":
				wait.until(ExpectedConditions.elementToBeClickable(FindAnElement(WebElement)));
				break;
			case "Presence":
				wait.until(ExpectedConditions.presenceOfElementLocated(getElementWithLocator(WebElement)));
				break;
			case "Visibility":
				wait.until(ExpectedConditions.visibilityOfElementLocated(getElementWithLocator(WebElement)));
				break;
			case "NotPresent":
				wait.until(ExpectedConditions.invisibilityOfElementLocated(getElementWithLocator(WebElement)));
				break;
			default:
				Thread.sleep(Time * 1000);
			}
		} catch (Exception e) {
			throw new IllegalArgumentException("wait For Condition \"" + TypeOfWait + "\" isn't supported.");
		}
	}

}