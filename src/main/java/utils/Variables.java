package utils;

import java.util.Properties;

public class Variables {
	
	public static Properties prop = new Properties();
	
	public String getReportConfigPath(){
		 String reportConfigPath = prop.getProperty("reportConfigPath");
		 if(reportConfigPath!= null) return reportConfigPath;
		 else throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath"); 
		}
	
	public static final String glue = "stepDefinations";
	public static final String tags = "@newsletter";
	public static final String ipaddress=  "?ip=116.240.204.120";
	public static final String ipaddress2=  "&ip=116.240.204.120";


	public static String CHROME = "chrome";
	public static String FIREFOX = "firefox";
	public static String APT_TITLE = "APT Luxury Travel, Touring & Cruising | Live Fully";
	public static String BOTANICA_TITLE = "Botanica World Discoveries";
	public static String TM_TITLE = "Travelmarvel Tours & Cruises";
	public static String CC_TITLE = "Captain's Choice - Life Enriching Journeys";
	public static String USER_DIR = "user.dir";
	public static long EXPLICIT_WAIT_TIME = 100;
	public static String SEARCH_RESULTS = "Search Results";
	public static String ROOMTYPE_SINGLE = "SINGLE";
	public static String CARD_NUMBER_FRAME_NAME = "braintree-hosted-field-number";
	public static String EXPIRATION_FRAME_NAME = "braintree-hosted-field-expirationDate";
	public static String CVV_FRAME_NAME = "braintree-hosted-field-cvv";
	public static String RIVER_CRUISE = "River Cruise";
	public static String LAND_JOURNEY = "landJourney";
	public static String AIR_TOUR = "Air tour";
	public static String OCEAN_CRUISE = "Ocean Cruise";
	public static String RAIL_JOURNEY = "Rail Journey";
	public static String THANK_YOU_FOR_SIGNING_UP_TO_OUR_NEWSLETTER = "THANK YOU FOR SIGNING UP TO OUR NEWSLETTER";
	public static String BOTANICA_THANK_YOU_FOR_SIGNING_UP_TO_OUR_NEWSLETTER = "Thank you for signing up to our newsletter";
	public static String BOOKING_SUCCESSFUL = "Booking Successful";
	public static String ID = "id";
	public static String URL = "url";
	public static String SITECORE_URL = "sitecoreurl";
	public static String BOTANICA_URL = "botanicaurl";
	public static String TM_URL = "travelmarvelurl";
	public static String SALESFORCE_URL = "salesforceurl";
	public static String AUSTRALIA = "Australia";
	public static String INDIA = "India";
	public static String CC_URL = "ccurl";
	public static String CANADA = "Canada";
	public static String EUROPE = "Europe";
	public static String BROWSER = "browser";
	public static String REQUEST_BROCHURE = "Request Brochure via Post";
	public static String BOTANICA_REQUEST_BROCHURE = "Free Botanica Brochures, Send Via Post, River Cruising Brochure";
	public static String TM_REQUEST_BROCHURE = "Free Travelmarvel Brochures, Send Via Post, River Cruising Brochure";
	public static String CONATINS_STRING_PREFIX = "//*[contains(text(), '";
	public static String CONATINS_STRING_SUFFIX = "')]";
	public static String DEBIT = "Debit";
	public static String IFRAME = "iframe";
	public static String EVERYTHING = "Everything";
	public static String TWO_WEEKS = "two weeks";
	public static String A_WEEK = "a week";
	public static String QUOTE = "Request Quote";
	public static String ANY_DURATION = "Any duration";
	public static String TRIP = "Trip";
	public static String TM = "TM";
	public static String ALL_REGIONS = "All regions";
	public static String LANGUAGE_FRAME = "Header_Language_Gallery";
	public static String SITECORE = "SiteCore";
	public static String APT = "APT";
	public static String SALESFORCE = "SalesForce";
	public static String BOTANICA = "Botanica";
	public static String CC = "Captain Choice";
	public static String CONFIG_PATH = "/scr/test/resources/properties/config.properties";
	public static String SPAN_TAG = "span";
	public static String COMMA_SEPERATOR = ",";
	public static String SPACE_SEPERATOR = " ";
	public static String FULL_AMOUNT = "full_amount";
	public static String TRIPSandQUOTE= "//*[@class = 'btn btn--primary trip-card__info'] | //*[@class = 'btn btn--primary trip-card__info ng-scope']";


}
