package utils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LogHelper {
	private static boolean root = false;

	public static Logger getLogger(Class clas) {
		if (root) {
			return Logger.getLogger(clas);
		}
		PropertyConfigurator
				.configure(System.getProperty(Variables.USER_DIR) + "/src/test/resources/properties/log4j.properties");
		root = true;
		return Logger.getLogger(clas);
	}
}
