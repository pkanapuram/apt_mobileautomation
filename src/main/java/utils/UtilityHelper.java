package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import base.TestBase;

public class UtilityHelper extends TestBase {

	public static Properties readPropertyFile(String file) {
		InputStream input = null;
		Properties prop = null;
		try {
			input = new FileInputStream(file);
			prop = new Properties();
			// load a properties file
			prop.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}

	public static String selectCategory(String Category) {
		String cabin = null;
		switch (Category) {
		case "Royal Suite":
			cabin = "Royal Suite";
			break;
		case "Category E":
			cabin = "Category E";
			break;
		case "Category D":
			cabin = "Category D";
			break;
		case "Category DD":
			cabin = "Category DD";
			break;
		case "Category C":
			cabin = "Category C";
			break;
		case "Category CC":
			cabin = "Category CC";
			break;
		case "Category B+":
			cabin = "Category B+";
			break;
		case "Category T":
			cabin = "Category T";
			break;
		case "Category TT":
			cabin = "Category TT";
			break;
		case "Category T+":
			cabin = "Category T+";
			break;
		case "Category TT+":
			cabin = "Category TT+";
			break;
		case "Category P":
			cabin = "Category P";
			break;
		case "Category P+":
			cabin = "Category P+";
			break;
		case "Owners Suite":
			cabin = "Owners Suite";
			break;
		case "Owners Suite+":
			cabin = "Owners Suite+";
			break;
		case "Deluxe Cabin":
			cabin = "Deluxe Cabin";
			break;
		case "Category 2 Twin":
			cabin = "Category 2 Twin";
			break;
		case "Deluxe Suite":
			cabin = "Deluxe Suite";
			break;
		case "Standard Suite":
			cabin = "Standard Suite";
			break;
		case "Island Suite":
			cabin = "Island Suite";
			break;
		case "Owners Balcony Suite":
			cabin = "Owners Balcony Suite";
			break;
		case "Sadec Suite":
			cabin = "Sadec Suite";
			break;
		case "Category A":
			cabin = "Category A";
			break;
		case "Master Suite":
			cabin = "Master Suite";
			break;
		case "Suite":
			cabin = "Suite";
			break;
		}
		return cabin;
	}

	public static void sleep() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}

	public static String containsString(String string, String tag) {
		return "//" + tag + "[contains(text(), '" + string + "')]";
	}
}
